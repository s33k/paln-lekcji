import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';


class LinkTile extends StatefulWidget {
  @override
  State createState() => LinkTileState();
}

class LinkTileState extends State<LinkTile> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  SettingsBloc bloc;
  String _link = "";

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SettingsBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: bloc.state,
        builder: (context, snapshot) {
          _link = snapshot.connectionState == ConnectionState.active && (snapshot.data as SettingsModel).url != null
              ? (snapshot.data as SettingsModel).url
              : _link;
          return ListTile(
            title: new Text("Link do planu"),
            trailing: Container(
              width: 200,
              child: new Text(
                _link,
                style: Theme.of(context).textTheme.subhead,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                softWrap: false,
              ),
            ),
            onTap: _showBottomSheetField,
          );
        });
  }

  void _showBottomSheetField() {
    showModalBottomSheet<String>(
        context: context,
        builder: (BuildContext context) {
          TextEditingController _controller = new TextEditingController(text: _link);
          _controller.addListener((){
            bloc.addUrlValidatorEvent(CheckUrl(_controller.value.text));
          });

          String linkToSave;

          return Container(
            height: MediaQuery.of(context).viewInsets.bottom + 100,
            child: StreamBuilder<Object>(
              stream: bloc.urlValidatorState,
              builder: (context, snapshot) {
                return Form(
                  key: _formKey,
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 375.0,
                        child: new TextFormField(
                          style: Theme.of(context).textTheme.title,
                          controller: _controller,
                          autofocus: true,
                          autovalidate: true,
                          onSaved: (value) {
                            linkToSave = value;
                          },
                          validator: (value) {
                            if(snapshot.data == "" || snapshot.data == null || snapshot.data == true){

                            }else{
                              return snapshot.data;
                            }
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        child: FlatButton(
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              Navigator.of(context).pop(linkToSave);
                            }
                          },
                          child: new Text("Zapisz"),
                        ),
                      ),
                    ],
                  ),
                );
              }
            ),
          );
        }).then((value) => _setLink(value));
  }

  void _setLink(String value) {
    if (value != null) {
      bloc.addUrlValidatorEvent(SubmitUrl(value));
    }
  }
}
