import 'package:flutter/material.dart';
import 'package:timetable/src/code/class.dart';

class ClassesList extends StatefulWidget {
  final List<Class> classesList;
  final void Function(Class) onChange;

  const ClassesList({Key key, this.classesList, this.onChange})
      : super(key: key);

  @override
  State createState() => ClassesListState();
}

class ClassesListState extends State<ClassesList> {
  Class _selectedClass;

  @override
  Widget build(BuildContext context) {
    List<Class> classesList = widget.classesList;

    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overScroll){
        overScroll.disallowGlow();
      },
      child: ListView.builder(
        itemCount: widget.classesList.length,
        itemBuilder: (BuildContext context, int index) {
          return new RadioListTile<Class>(
            title: Text(classesList[index].number),
            value: classesList[index],
            groupValue: _selectedClass,
            selected: _selectedClass == classesList[index],
            onChanged: (value) {
              setState(() {
                _selectedClass = value;
                widget.onChange(value);
              });
            },
          );
        },
      ),
    );
  }
}
