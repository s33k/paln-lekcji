import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/theme/bloc.dart';

class ThemeTile extends StatefulWidget {
  @override
  State createState() => new ThemeTileState();
}

class ThemeTileState extends State<ThemeTile> with TickerProviderStateMixin {
  ThemeBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ThemeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: new Text("Motyw"),
      trailing: StreamBuilder(
          initialData: bloc.initialData,
          stream: bloc.themeStream,
          builder: (context, snapshot) {
            return DropdownButton(
              hint: Text("Wybierz motyw"),
              value: snapshot.data.theme as Themes,
              onChanged: _changeTheme,
              items: _getDropdownOptions(),
            );
          }),
    );
  }

  void _changeTheme(Themes theme) {
    bloc.add(ChangeThemeEvent(theme));
  }

  List<DropdownMenuItem<Themes>> _getDropdownOptions() {
    Map<String, String> translate = {"Default": "Domyślny", "Dark": "Ciemny"};
    List<DropdownMenuItem<Themes>> returnList = [];

    themesNames.forEach((theme, name) {
      returnList.add(new DropdownMenuItem(
        value: theme,
        child: new Text(translate.containsKey(name) ? translate[name] : name),
      ));
    });

    return returnList;
  }
}
