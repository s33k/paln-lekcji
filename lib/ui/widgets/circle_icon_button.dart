import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class CircleIconButton extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData icon;
  final Color color;
  final Padding padding;

  const CircleIconButton(
      {Key key,
      @required this.onPressed,
      @required this.icon,
      this.color,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.all(25.0),
      child: Container(
        height: 50.0,
        width: 50.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(90),
          border: Border.all(
              color: color ?? Theme.of(context).primaryColor, width: 2.0),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(90),
          onTap: onPressed,
          child: Icon(
            icon,
            color: color ?? Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }
}
