import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';

class MyDrawer extends StatefulWidget {
  final Page selectedPage;
  final Function onSettingsOpen, onSettingsClose;

  const MyDrawer({Key key, this.selectedPage, this.onSettingsOpen, this.onSettingsClose}) : super(key: key);

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  SettingsBloc settingsBloc;

  @override
  void initState() {
    settingsBloc = BlocProvider.of<SettingsBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: Drawer(
        child: ListView(
          children: <Widget>[
            StreamBuilder(
              stream: settingsBloc.state,
              builder: (BuildContext context,
                  AsyncSnapshot<SettingsModel> snapshot) {
                String classNumber = '', url = '';

                if (snapshot.connectionState == ConnectionState.active) {
                  classNumber = snapshot.data.selectedClass.number;
                  url = snapshot.data.url;
                }

                return Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          '$classNumber',
                          style: Theme.of(context).textTheme.display1,
                        ),
                        Text(
                          '$url',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    border: Border(),
                  ),
                  constraints: BoxConstraints(minHeight: 150),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.event_note),
              title: Text("Plan lekcji"),
              selected: true,
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("Ustawienia"),
              onTap: () {
                widget.onSettingsOpen();
                Navigator.of(context).pushNamed('settings').then((_){
                  widget.onSettingsClose();
                });
              },
            )
          ],
        ),
      ),
    );
  }
}

enum Page { TIMETABLE, SETTINGS }
