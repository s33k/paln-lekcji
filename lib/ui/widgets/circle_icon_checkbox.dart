import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class CircleIconCheckBox extends StatefulWidget{

  final void Function(bool) onChanged;
  final bool value;
  final IconData icon;
  final Color color, backgroundColor;
  final EdgeInsetsGeometry padding;

  const CircleIconCheckBox({Key key, this.onChanged, @required this.icon, this.color, this.padding, @required this.value, this.backgroundColor}) : super(key: key);

  @override
  State createState() => CircleIconCheckBoxState();
}

class CircleIconCheckBoxState extends State<CircleIconCheckBox> with TickerProviderStateMixin {

  bool _value;
  Color _color, _backgroundColor;
  EdgeInsetsGeometry _padding;
  bool _enable;
  AnimationController _animationController;
  Animation<Color> _containerBackgroundAnimation, _iconColorAnimation;
  Animation _iconRotationAnimation;

  @override
  void initState() {
    super.initState();
    _value = widget.value;
    _color = widget.color;
    _backgroundColor = widget.backgroundColor;
    _padding = widget.padding ?? EdgeInsets.all(25.0);
    _enable = widget.onChanged != null;

    _animationController = new AnimationController(vsync: this, duration: Duration(milliseconds: 750), value: _value ? 1.0 : 0.0);
    _containerBackgroundAnimation = new ColorTween(begin: Colors.transparent, end: _color ?? Colors.red).animate(_animationController)..addListener(() => setState((){}));
    _iconColorAnimation = new ColorTween(begin: _color ?? Colors.red, end: _backgroundColor ?? Colors.white).animate(_animationController)..addListener(() => setState((){}));
    _iconRotationAnimation = new CurveTween(curve: Curves.easeInOut).animate(_animationController)..addListener(() => setState((){}));
  }

  @override
  Widget build(BuildContext context) {
    _color = _color ?? Theme.of(context).primaryColor;
    _backgroundColor = _backgroundColor ?? Theme.of(context).canvasColor;

    return Padding(
      padding: _padding,
      child: Container(
        height: 50.0,
        width: 50.0,
        decoration: BoxDecoration(
          color: _containerBackgroundAnimation.value,
          borderRadius: BorderRadius.circular(90),
          border: Border.all(
              color: _enable ? _color : Theme.of(context).disabledColor, width: 2.0),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(90),
          onTap: (){
            if(_enable){
              setState(() {
                _value = !_value;
              });
              _animationController.isCompleted ? _animationController.reverse() : _animationController.forward();
              widget.onChanged(_value);
            }
          },
          child: Transform.rotate(
            angle: _iconRotationAnimation.value * 6.3,
            child: Icon(
              widget.icon,
              color: _enable ? _iconColorAnimation.value : Theme.of(context).disabledColor,
            ),
          ),
        ),
      ),
    );
  }
}