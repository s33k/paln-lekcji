import 'package:flutter/material.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/day.dart';

class FreeTimeTile extends StatefulWidget{

  final Hour duration;
  final String day;

  const FreeTimeTile({Key key, this.duration, this.day}) : super(key: key);

  @override
  State createState() => FreeTimeTileState();
}

class FreeTimeTileState extends State<FreeTimeTile>{

  Hour duration;
  String _day;
  int styleIndex = 2;


  @override
  void initState() {
    super.initState();
    duration = widget.duration;
    _day = widget.day;

    if(DateTime.now().weekday == DayNames().english.indexOf(_day) + 1){
      var time = new Duration(hours: DateTime.now().hour, minutes: DateTime.now().minute);
      if(time >  duration.end){
        styleIndex = 0;
      }else if(time > duration.start){
        styleIndex = 1;
      }else{
        styleIndex = 2;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    List<TextStyle> styles = [
        Theme.of(context).textTheme.subhead.copyWith(decoration: TextDecoration.lineThrough, decorationColor: Theme.of(context).primaryColor, color: Theme.of(context).disabledColor),
        Theme.of(context).textTheme.subhead.copyWith(color: Theme.of(context).primaryColor),
        Theme.of(context).textTheme.subhead
    ];
    return ListTile(
      title: Text("Masz okienko od ${duration.getStartText()} do ${duration.getEndText()}", style: styles[styleIndex],),
    );
  }
}