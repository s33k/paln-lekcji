import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';
import 'package:timetable/ui/screnns/select_class_screen.dart';

class ClassTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SettingsBloc bloc = BlocProvider.of<SettingsBloc>(context);

    return StreamBuilder<Object>(
      stream: bloc.state,
      builder: (context, snapshot) {
        bool active = snapshot.connectionState == ConnectionState.active;
        return ListTile(
          selected: active && (snapshot.data as SettingsModel).selectedClass == null,
          subtitle: active && (snapshot.data as SettingsModel).selectedClass == null ? new Text("Musisz wybrać klasę") : null,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => SelectClass(onSelect: (){
              Navigator.of(context).pop();
            },)));
          },
          title: new Text("Wybrana klasa"),
          trailing: Container(width: 200, child: active ? Text((snapshot.data as SettingsModel).selectedClass != null ? (snapshot.data as SettingsModel).selectedClass.number + " ▶" : "WYBIERZ KLASĘ", softWrap: false, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end,) : Container(constraints: BoxConstraints.loose(Size.zero),)),
        );
      }
    );
  }
}
