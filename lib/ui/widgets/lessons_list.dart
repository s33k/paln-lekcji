import 'package:flutter/material.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/ui/widgets/free_time_tile.dart';
import 'package:timetable/ui/widgets/lesson_tile.dart';

class LessonsList extends StatefulWidget {

  final List<Lesson> lessons;
  final Hour hour;
  final bool hourEnabled;

  const LessonsList({Key key, this.lessons, this.hour, this.hourEnabled}) : super(key: key);

  @override
  State createState() => new LessonsListState();
}

class LessonsListState extends State<LessonsList>
    with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overScroll) {
        overScroll.disallowGlow();
      },
      child: new ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        itemCount: widget.lessons.length,
        itemBuilder: (BuildContext context, int index) {
          Lesson actualLesson = widget.lessons[index];
          List<Widget> columnWidgets = [];

          columnWidgets.add(LessonTile(lesson: actualLesson, hour: widget.hour, hourEnabled: widget.hourEnabled));

          if(actualLesson != widget.lessons.last){
            if(actualLesson.hour.number + 1 != widget.lessons[index + 1].hour.number && actualLesson.hour.number != widget.lessons[index + 1].hour.number){
              Hour freeTime = new Hour(0, actualLesson.hour.end, widget.lessons[index + 1].hour.start);
              columnWidgets.add(Divider(height: 0.0,));
              columnWidgets.add(FreeTimeTile(duration: freeTime, day: actualLesson.day));
            }
          }

          if(index != widget.lessons.length - 1 && actualLesson.hour.number != widget.lessons[index + 1].hour.number){
            columnWidgets.add(Divider(height: 0.0,));
          }

          if(columnWidgets.length == 1){
            return columnWidgets.first;
          }else{
            return Column(
              children: columnWidgets,
            );
          }
        },
      ),
    ));
  }

  @override
  bool get wantKeepAlive => true;
}
