import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';
import 'package:timetable/src/helpers/download_helper.dart';
import 'package:timetable/ui/widgets/circle_icon_button.dart';
import 'package:timetable/ui/widgets/circle_icon_checkbox.dart';
import 'package:timetable/ui/widgets/lesson_dialog.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

// ignore: must_be_immutable
class TimetableBottomMenu extends StatelessWidget {
  final VoidCallback onSettingsOpen, onSettingsClose;
  VoidCallback _closeCallback;
  ScheduleBloc bloc;
  GlobalKey addLessonKey = new GlobalKey(),
      downloadKey = new GlobalKey(),
      showMyLessonsKey = new GlobalKey(),
      selectMyLessonsKey = new GlobalKey(),
      infoKey = new GlobalKey(),
      settingsKey = new GlobalKey();

  TimetableBottomMenu({Key key, this.onSettingsOpen, this.onSettingsClose})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<ScheduleBloc>(context);

    return Container(
      height: 185.0,
      decoration: BoxDecoration(
          color: Theme.of(context).bottomAppBarColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 10.0,
          ),
          Container(
            height: 25,
            alignment: Alignment(0.9, 0),
            child: FlatButton.icon(
              icon: Icon(
                Icons.help_outline,
                color: Theme.of(context).primaryColor,
              ),
              label: Text(
                "Pomoc",
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: Theme.of(context).primaryColor),
              ),
              onPressed: () {
                _showTutorial(context);
              },
            ),
          ),
          Container(
            height: 100.0,
            child: NotificationListener<OverscrollIndicatorNotification>(
              // ignore: missing_return
              onNotification: (notification) {
                notification.disallowGlow();
              },
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Tooltip(
                    key: addLessonKey,
                    child: CircleIconButton(
                      icon: Icons.add,
                      onPressed: () async {
                        await addLessonDialog(context);
                      },
                    ),
                    message: "Dodaj lekcję",
                  ),
                  Tooltip(
                    key: downloadKey,
                    child: CircleIconButton(
                      icon: Icons.file_download,
                      onPressed: () async {
                        await showDownloadDialog(context);
                      },
                    ),
                    message: "Pobierz plan",
                  ),
                  StreamBuilder(
                      stream: bloc.showLessonState,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.active) {
                          return Tooltip(
                            key: showMyLessonsKey,
                            child: CircleIconCheckBox(
                              icon: Icons.directions_run,
                              onChanged: (value) {
                                if (value) {
                                  bloc.add(ShowOnlyMyLessons());
                                } else {
                                  bloc.add(ShowAllLessons());
                                }
                                bloc.add(LoadSchedule());
                              },
                              value: snapshot.data,
                              color: Theme.of(context).primaryColor,
                              backgroundColor: Theme.of(context).canvasColor,
                            ),
                            message: "Pokaż tylko lekcje na które chodzę",
                          );
                        } else {
                          return CircleIconButton(
                            icon: null,
                            onPressed: () {},
                          );
                        }
                      }),
                  Tooltip(
                    key: selectMyLessonsKey,
                    child: CircleIconButton(
                      icon: Icons.sync,
                      onPressed: () {
                        Navigator.of(context)
                            .pushNamed('select my lessons')
                            .then((_) {
                          bloc.add(LoadSchedule());
                        });
                      },
                    ),
                    message: "Wybierze lekcje na które chodzisz",
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                key: infoKey,
                icon: Icon(Icons.info_outline),
                color: Theme.of(context).primaryColor,
                onPressed: () async {
                  await showTimetableInfoDialog(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.close),
                color: Theme.of(context).primaryColor,
                onPressed: _closeCallback,
              ),
              IconButton(
                key: settingsKey,
                icon: Icon(Icons.settings),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  onSettingsOpen();
                  Navigator.of(context).pushNamed('settings').then((_) {
                    onSettingsClose();
                  });
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  void setOnClose(VoidCallback close) {
    _closeCallback = close;
  }

  Future showTimetableInfoDialog(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Informacje"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.5)),
            content: FutureBuilder(
                future: bloc.state.first,
                builder: (context, AsyncSnapshot<ScheduleModel> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Klasa: ${snapshot.data.actuallyClass.number}"),
                        Text("Link: ${snapshot.data.actuallyUrl}"),
                        snapshot.data.beginDate != null
                            ? Text(snapshot.data.beginDate)
                            : Container(),
                        snapshot.data.generateDate != null
                            ? Text(snapshot.data.generateDate)
                            : Container(),
                      ],
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                }),
            actions: <Widget>[
              FlatButton(
                child: Text("Anuluj"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  Future showDownloadDialog(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          Widget _showMessage(IconData icon, String text, Color color) {
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Icon(
                    icon,
                    color: color,
                    size: 60.0,
                  ),
                  Text(
                    text,
                    style: TextStyle(color: color),
                  )
                ],
              ),
            );
          }

          FutureBuilder checkTimetable = new FutureBuilder(
              future: DownloadHelper().checkSchedule(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      return _showMessage(
                          Icons.error_outline,
                          snapshot.error.toString(),
                          Theme.of(context).errorColor);
                    } else {
                      if (snapshot.data != null) {
                        if (snapshot.data) {
                          return _showMessage(Icons.check_circle_outline,
                              "Masz aktualny plan", Colors.green);
                        } else {
                          return _showMessage(
                              Icons.error_outline,
                              "Jest nowa wesja planu do pobrania",
                              Colors.amber);
                        }
                      }
                      return Text("Nie masz jeszcze pobranego plnau.");
                    }
                    break;
                  default:
                    return Container();
                }
              });

          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.5)),
            title: Text(
              "Pobierz plan",
              style: Theme.of(context)
                  .textTheme
                  .title
                  .copyWith(color: Theme.of(context).primaryColor),
            ),
            content: Container(height: 100.0, child: checkTimetable),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "Anuluj",
                    style: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: Theme.of(context).disabledColor),
                  )),
              FlatButton(
                  onPressed: () async {
                    bloc.add(DownloadSchedule());
                    Navigator.of(context).pop();
                  },
                  child: Text("Pobierz")),
            ],
          );
        });
    return;
  }

  void _showTutorial(BuildContext context){
    List<TargetFocus> targets = [];

    targets.add(TargetFocus(
      identify: "add",
      keyTarget: addLessonKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Dodaj lekcje ręcznie", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));
    targets.add(TargetFocus(
      identify: "download",
      keyTarget: downloadKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Pobierz plan", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));
    targets.add(TargetFocus(
      identify: "show lessons",
      keyTarget: showMyLessonsKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Włącz pokazywanie tylko moich lekji", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));
    targets.add(TargetFocus(
      identify: "select lessons",
      keyTarget: selectMyLessonsKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Wybierz lekcje na które chodzisz. Resztę będziesz mógł schować.", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));
    targets.add(TargetFocus(
      identify: "info",
      keyTarget: infoKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Informacje o planie.", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));
    targets.add(TargetFocus(
      identify: "settings",
      keyTarget: settingsKey,
      contents: [
        ContentTarget(
          align: AlignContent.top,
          child: Text("Otwórz ustawienia.", style: Theme.of(context).textTheme.display1,)
        )
      ]
    ));

    TutorialCoachMark(
      context,
      targets: targets,
      textSkip: "Pomiń",
      alignSkip: Alignment(1, 1),
      paddingFocus: 0
    )..show();
  }
}
