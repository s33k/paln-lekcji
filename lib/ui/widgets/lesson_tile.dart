import 'package:flutter/material.dart';
import 'package:timetable/src/code/hour.dart';

import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/ui/widgets/lesson_dialog.dart';

class LessonTile extends StatelessWidget{

  final Lesson lesson;
  final Hour hour;
  final bool hourEnabled;

  const LessonTile({Key key, this.lesson, this.hour, this.hourEnabled}) : super(key: key);

  @override
    Widget build(BuildContext context) {
      LessonStatus lessonStatus;
      if(hour != null){
        if(lesson.hour == hour){
          lessonStatus = LessonStatus.NOW;
        }else if(lesson.hour.start > hour.end){
          lessonStatus = LessonStatus.FUTURE;
        }else{
          lessonStatus = LessonStatus.PAST;
        }
      }else if(hourEnabled){
        lessonStatus = LessonStatus.PAST;
      }

      var _textStyle = {
        LessonStatus.PAST: Theme.of(context).textTheme.subhead.copyWith(decoration: TextDecoration.lineThrough, decorationColor: Theme.of(context).primaryColor, color: Theme.of(context).disabledColor),
        LessonStatus.NOW: Theme.of(context).textTheme.subhead.copyWith(color: Theme.of(context).primaryColor),
        LessonStatus.FUTURE: Theme.of(context).textTheme.subhead
      };

      double _visible = 0.65;

      Color _iconColor = Theme.of(context).iconTheme.color.withOpacity(_visible);
      TextStyle _tooltipTextStyle = Theme.of(context).textTheme.body1.copyWith(color: Theme.of(context).textTheme.body1.color.withOpacity(_visible));

      return ExpansionTile(
                backgroundColor: Colors.transparent,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(lesson.subject, style: _textStyle[lessonStatus],),
                        Text(lesson.group, style: _textStyle[lessonStatus],)
                      ],
                    ),
                    Text(lesson.hour.getText(), style: Theme.of(context).textTheme.subtitle.copyWith(color: Theme.of(context).disabledColor),)
                  ],
                ) ,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.symmetric(horizontal: 0.0),
                    child:new Row(
                      children: <Widget>[
                        Expanded(
                          flex: 7,
                          child: Row(
                            children: <Widget>[
                              lesson.teacher != "" ? Container(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15.0, right: 5.0),
                                  child: new Tooltip(
                                    message: lesson.teacher,
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.person, size: 24.0, color: _iconColor,),
                                        Text(lesson.teacher, style: _tooltipTextStyle,),
                                      ],
                                    ),
                                  ),
                                ),
                              ) : Padding(padding: EdgeInsets.only(left: 15.0),),
                              Container(
                                child: new Tooltip(
                                  message: lesson.room,
                                  child: Row(
                                    children: <Widget>[
                                      Icon(Icons.place, size: 24.0, color: _iconColor,),
                                      Text(lesson.room, style: _tooltipTextStyle,)
                                    ],
                                  ),
                                ),
                              ),
                              new Spacer(flex: 3,)
                            ],
                          ),
                        ),
                        new Expanded(
                          flex: 1,
                            child: new IconButton(
                              icon: Icon(Icons.edit, size: 24.0, color: _iconColor,),
                                onPressed: () async{
                                  await editLessonDialog(context, lesson);
                                }),
                            ),
                      ],
                    ),
                  )
                ],
              );
    }
}