import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';

import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/helpers/database_helper.dart';

class LessonDialog extends StatefulWidget {
  final Lesson lesson;

  const LessonDialog({Key key, this.lesson}) : super(key: key);

  @override
  State createState() => new LessonDialogState();
}

class LessonDialogState extends State<LessonDialog> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScheduleBloc bloc;

  String _subject;
  String _teacher;
  String _room;
  String _group;
  String _day;
  int _id;
  bool _show;
  Hour _hour;
  List<dynamic> hoursList = [];

  final days = {
    "MONDAY": "Poniedziałek",
    "TUESDAY": "Wtorek",
    "WEDNESDAY": "Środa",
    "THURSDAY": "Czwartek",
    "FRIDAY": "Piątek",
  };

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ScheduleBloc>(context);
    if (widget.lesson != null) {
      _subject = widget.lesson.subject;
      _teacher = widget.lesson.teacher;
      _room = widget.lesson.room;
      _group = widget.lesson.group;
      _day = widget.lesson.day ?? "MONDAY";
      _id = widget.lesson.id;
      _show = widget.lesson.show ?? false;
      _hour = widget.lesson.hour;
    } else {
      _group = "";
      _day = "MONDAY";
      _show = true;
    }

    _getHoursList();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Lekcja",
          style: Theme.of(context)
              .textTheme
              .title
              .copyWith(color: Theme.of(context).primaryColor)),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(7.5))),
      contentPadding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      content: Container(
        constraints: BoxConstraints.expand(width: 350),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: ClipRect(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Form(
                          key: formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                initialValue: _subject,
                                textAlign: TextAlign.end,
                                decoration: InputDecoration(icon: Container(child: Text("Przedmiot: "), constraints: BoxConstraints(minWidth: 150))),
                                validator: (value) {
                                  if (value.isEmpty) return "Muszisz wpisać przedmoit";
                                },
                                onSaved: (value) {
                                  setState(() {
                                    _subject = value;
                                  });
                                },
                              ),TextFormField(
                                initialValue: _teacher,
                                textAlign: TextAlign.end,
                                decoration: InputDecoration(icon: Container(child: Text("Nauczyciel: "), constraints: BoxConstraints(minWidth: 150))),
                                onSaved: (value) {
                                  setState(() {
                                    _teacher = value;
                                  });
                                },
                              ),Container(
                                child: TextFormField(
                                  initialValue: _room,
                                  textAlign: TextAlign.end,
                                  decoration: InputDecoration(icon: Container(child: Text("Sala: "), constraints: BoxConstraints(minWidth: 150))),
                                  onSaved: (value) {
                                    setState(() {
                                      _room = value;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Grupa: "),
                            InkWell(
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0.0, 10.0, 10.0, 15.0),
                                child:
                                    Text("${_group == "" ? "Cała klasa" : _group}"),
                              ),
                              onTap: () {
                                new Picker(
                                    backgroundColor:
                                        Theme.of(context).dialogBackgroundColor,
                                    textStyle: Theme.of(context).textTheme.title,
                                    adapter: NumberPickerAdapter(data: [
                                      NumberPickerColumn(
                                        begin: 0,
                                        end: 10,
                                        initValue: int.parse(
                                            _group.isEmpty ? "0" : _group[0]),
                                      ),
                                      NumberPickerColumn(
                                        begin: 0,
                                        end: 10,
                                        initValue: int.parse(
                                            _group.isEmpty ? "0" : _group[2]),
                                      ),
                                    ]),
                                    title: Text(
                                      "Wybierz grupę",
                                      style: Theme.of(context)
                                          .textTheme
                                          .title
                                          .copyWith(
                                              color: Theme.of(context).primaryColor),
                                    ),
                                    hideHeader: true,
                                    delimiter: [
                                      PickerDelimiter(
                                          child: Container(
                                        width: 30.0,
                                        alignment: Alignment.center,
                                        child: Text(
                                          "/",
                                          style: TextStyle(fontSize: 36.0),
                                        ),
                                      ))
                                    ],
                                    cancelText: "Anuluj",
                                    confirmText: "Wybierz",
                                    onConfirm: (picker, value) {
                                      setState(() {
                                        if (value[0] != 0 && value[1] != 0) {
                                          _group = "${value[0]}/${value[1]}";
                                        } else {
                                          _group = "";
                                        }
                                      });
                                    }).showDialog(context);
                              },
                              borderRadius: BorderRadius.circular(2.5),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Czas: "),
                            InkWell(
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0.0, 10.0, 10.0, 15.0),
                                child: Text(
                                    "${days[_day]} lekcja ${_hour != null ? _hour.number.toString() : "1"}"),
                              ),
                              onTap: () {
                                new Picker(
                                    backgroundColor:
                                        Theme.of(context).dialogBackgroundColor,
                                    textStyle: Theme.of(context).textTheme.title,
                                    adapter: PickerDataAdapter(isArray: true, data: [
                                      PickerItem(
                                        children: _daysList(),
                                      ),
                                      PickerItem(
                                        children: _hourListPickers(),
                                      ),
                                    ]),
                                    changeToFirst: true,
                                    title: Text(
                                      "Wybierz czas",
                                      style: Theme.of(context)
                                          .textTheme
                                          .title
                                          .copyWith(
                                              color: Theme.of(context).primaryColor),
                                    ),
                                    hideHeader: true,
                                    cancelText: "Anuluj",
                                    confirmText: "Wybierz",
                                    selecteds: [
                                      days.keys.toList().indexOf(_day),
                                      _hour.number - 1
                                    ],
                                    onConfirm: (picker, value) {
                                      setState(() {
                                        _day = days.keys.toList()[value[0]];
                                        _hour = hoursList[value[1]];
                                      });
                                    }).showDialog(context);
                              },
                              borderRadius: BorderRadius.circular(2.5),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Chodzisz na lekcję:"),
                            Switch(
                                value: _show,
                                onChanged: (value) => setState(() => _show = value))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _id != null
                    ? IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () async{
                          bloc.add(DeleteLesson(widget.lesson));
                          Navigator.pop(context);
                        })
                    : Container(),
                ButtonTheme.bar(
                    child: ButtonBar(
                  children: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("Anuluj",
                            style: Theme.of(context).textTheme.button.copyWith(
                                color: Theme.of(context).disabledColor))),
                    FlatButton(
                        onPressed: () {
                          _save();
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Zapisz",
                          style: Theme.of(context)
                              .textTheme
                              .button
                              .copyWith(color: Theme.of(context).primaryColor),
                        )),
                  ],
                )),
              ],
            )
          ],
        ),
      ),
    );
  }

  List<PickerItem> _daysList() {
    List<PickerItem> listToReturn = [];

    days.forEach((key, value) {
      listToReturn.add(PickerItem(text: Text(value), value: key));
    });
    return listToReturn;
  }

  void _getHoursList() async {
    hoursList = await DBHelper().getHours;
    setState(() {
      _hour = _hour ?? hoursList.first;
    });
  }

  List<PickerItem> _hourListPickers() {
    List<PickerItem> listToReturn = [];

    hoursList.forEach((hour) {
      listToReturn
          .add(PickerItem(text: Text(hour.number.toString()), value: hour));
    });
    return listToReturn;
  }

  void _save() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      Lesson lesson =
          new Lesson(_subject, _teacher, _room, _hour, _group, _day, _show);
      if (_id == null) {
        bloc.add(AddLesson(lesson));
      } else {
        lesson.id = _id;
        bloc.add(EditLesson(lesson));
      }
      bloc.add(LoadSchedule());
    }
  }
}

Future addLessonDialog(BuildContext context) async {
  await showDialog(
      context: context,
      builder: (BuildContext context) {
        return LessonDialog();
      });
  return;
}

Future editLessonDialog(BuildContext context, Lesson lesson) async {
  await showDialog(
      context: context,
      builder: (BuildContext context) {
        return LessonDialog(
          lesson: lesson,
        );
      });
}
