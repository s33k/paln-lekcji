import 'package:flutter/material.dart';
import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';

class SelectLessonCard extends StatefulWidget {
  final String day;
  final Hour hour;
  final Lesson select;
  final Function(Lesson) onSelect;
  final List<Lesson> lessons;

  const SelectLessonCard(
      {Key key, this.day, this.hour, this.lessons, this.select, this.onSelect})
      : super(key: key);

  @override
  _SelectLessonCardState createState() => _SelectLessonCardState();
}

class _SelectLessonCardState extends State<SelectLessonCard>
    with AutomaticKeepAliveClientMixin {
  String day;
  Lesson selected;

  @override
  void initState() {
    day = DayNames().englishPolish[widget.day];
    selected = widget.select;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
          minHeight: 450, minWidth: 325, maxWidth: 325, maxHeight: 500),
      decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          border: Border.all(color: Theme.of(context).cardColor),
          borderRadius: BorderRadius.circular(7.5),
        boxShadow: [
          BoxShadow(offset: Offset(5, 5), blurRadius: 5.0, color: selected != null ? Theme.of(context).highlightColor : Theme.of(context).backgroundColor)
        ]
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              day,
              style: Theme.of(context).textTheme.display1.copyWith(color: Theme.of(context).primaryColor),
            ),
            Text(
              widget.hour.getText(),
              style: Theme.of(context).textTheme.subhead.copyWith(fontSize: 21.0),
            ),
            SizedBox(
              height: 40,
            ),
            Builder(
              builder: (BuildContext context) {
                List<Widget> children = [];
                for (Lesson lesson in widget.lessons) {
                  bool thisIsSelected = false;
                  if (selected == lesson) thisIsSelected = true;
                  children.add(ListTile(
                    leading: Icon(Icons.book),
                    contentPadding: EdgeInsets.all(1.0),
                    title: Text(lesson.subject),
                    trailing: Text(
                      lesson.group,
                      style: thisIsSelected
                          ? Theme.of(context)
                              .textTheme
                              .body1
                              .copyWith(color: Theme.of(context).primaryColor)
                          : null,
                    ),
                    selected: thisIsSelected,
                    onTap: () {
                      setState(() {
                        selected = lesson;
                      });
                      widget.onSelect(lesson);
                    },
                  ));
                }
                children.add(ListTile(
                  leading: Icon(Icons.close),
                  contentPadding: EdgeInsets.all(1.0),
                  title: Text("Nie chodzę na żadną"),
                  selected: selected != null && selected.subject == '',
                  onTap: () {
                    setState(() {
                      selected = Lesson('', '', '',
                          Hour(0, Duration(), Duration()), '', '', false);
                    });
                    widget.onSelect(null);
                  },
                ));
                return Container(
                  constraints: BoxConstraints(maxWidth: 325, maxHeight: 320),
                  child: NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overScroll) {
                      overScroll.disallowGlow();
                    },
                    child: ListView(
                      children: children,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
