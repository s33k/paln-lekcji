import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/code/schedule.dart';
import 'package:timetable/ui/widgets/lesson_dialog.dart';

class LessonsTable extends StatefulWidget {
  final Schedule timetable;
  final Map<int, double> heights;
  final double containerHeight;
  final Hour hour;

  const LessonsTable(
      {Key key,
      this.heights,
      this.timetable,
      this.containerHeight,
      this.hour,})
      : super(key: key);

  @override
  State createState() => LessonsTableState();
}

class LessonsTableState extends State<LessonsTable> {
  static const double COLUMN_WIDTH = 110;

  Map<int, double> _heights = {};
  double _containerHeight;
  List<Day> days;
  List<dynamic> hours;
  Hour _selectedHour;
  int _selectedDay;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now().weekday;
  }

  @override
  Widget build(BuildContext context) {
    _selectedHour = widget.hour ?? Hour(0, Duration(), Duration());
    days = List.from(widget.timetable.allDays.values);
    hours = widget.timetable.allHours;
    _containerHeight = widget.containerHeight;
    _heights = widget.heights;

    return _createTable();
  }

  Widget _createTable() {
    List<Widget> mainChildren = [];

    mainChildren.add(Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: _daysNameList(),
      ),
    ));

    for (int hour = 0; hour < hours.length; hour++) {
      List<Widget> rowWidgets = [];

      for (int day = 0; day < days.length; day++) {
        List<Lesson> lessons = days[day].lessons;
        var lessonsThatHour = lessons.where((lesson) {
          if (lesson.hour.number == hours[hour].number) return true;
          return false;
        });
        List<Widget> children = [];
        lessonsThatHour.forEach((lesson) {
          children.add(Card(
            margin: EdgeInsets.all(4.0),
            child: InkWell(
              onTap: () => _showLessonDialog(lesson),
              child: Container(
                height: (_heights[hours[hour].number] -
                        8.0 * lessonsThatHour.length) /
                    lessonsThatHour.length,
                child: Column(
                  children: <Widget>[
                    Expanded(child: AutoSizeText(lesson.subject)),
                    Expanded(child: AutoSizeText(lesson.group)),
                  ],
                ),
              ),
            ),
          ));
        });

        rowWidgets.add(Expanded(
            child: Container(
          color: day + 1 == _selectedDay
              ? Theme.of(context).primaryColorLight
              : Colors.transparent,
          constraints: BoxConstraints(maxHeight: _heights[hours[hour].number]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: children,
          ),
        )));
      }

      mainChildren.add(Container(
        color: hours[hour].number == _selectedHour.number
            ? Theme.of(context).primaryColorLight
            : Colors.transparent,
        child: Row(
          children: rowWidgets,
        ),
      ));
      mainChildren.add(Divider(
        height: 0.0,
        color: Theme.of(context).dividerColor,
      ));
    }

    return Container(
      height: _containerHeight,
      width: COLUMN_WIDTH * 5,
      child: Column(
        children: mainChildren,
      ),
    );
  }

  List<Widget> _daysNameList() {
    List<String> dayNames = [
      "Poniedziałek",
      "Wtorek",
      "Środa",
      "Czwartek",
      "Piątek"
    ];
    List<Widget> listToReturn = [];

    for (int i = 0; i < 5; i++) {
      listToReturn.add(
        Expanded(
            child: InkWell(
          onTap: () {
            setState(() => _selectedDay == i + 1
                ? _selectedDay = null
                : _selectedDay = i + 1);
          },
          child: Container(
              constraints: BoxConstraints(maxHeight: 50.0, minHeight: 50.0),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(color: Colors.transparent)),
              child: Center(
                  child: AutoSizeText(
                dayNames[i],
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 30.0),
                maxLines: 1,
              ))),
        )),
      );
    }

    return listToReturn;
  }

  void _showLessonDialog(Lesson lesson) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              lesson.subject,
              style: Theme.of(context)
                  .textTheme
                  .title
                  .copyWith(color: Theme.of(context).primaryColor),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(7.5))),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () async {
                    await editLessonDialog(context, lesson);
                    Navigator.pop(context);
                  })
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: lesson.teacher != ""
                      ? Row(
                          children: <Widget>[
                            Icon(Icons.person),
                            Text(lesson.teacher)
                          ],
                        )
                      : Container(),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: lesson.room != ""
                      ? Row(
                          children: <Widget>[
                            Icon(Icons.place),
                            Text(lesson.room)
                          ],
                        )
                      : Container(),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: lesson.group != ""
                      ? Row(
                          children: <Widget>[
                            Icon(Icons.group),
                            Text(lesson.group)
                          ],
                        )
                      : Container(),
                ),
              ],
            ),
          );
        });
  }
}
