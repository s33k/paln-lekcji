import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/ui/widgets/classes_list.dart';

class SelectClass extends StatefulWidget {

  final VoidCallback onSelect;

  const SelectClass({Key key, this.onSelect}) : super(key: key);

  @override
  State createState() => SelectClassState();
}

class SelectClassState extends State<SelectClass> with SingleTickerProviderStateMixin{
  SettingsBloc bloc;
  StreamBuilder _listBuilder;
  Class _selectedClass;
  AnimationController _controller;
  Animation backgroundAnimation, textAnimation;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SettingsBloc>(context);
    bloc.addClassEvent(LoadClassList());
    _listBuilder = new StreamBuilder(
        stream: bloc.classListState,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              if (snapshot.data is List) {
                List<Class> classesList = snapshot.data;
                return ClassesList(classesList: classesList, onChange: (value) {
                  setState(() {
                    _selectedClass = value;
                    _controller.forward();
                  });
                },);
              }else{
                return Text(snapshot.data.toString());
              }
              break;
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.done:
              return CircularProgressIndicator();
              break;
          }
          return Container();
        });
    _controller = new AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    backgroundAnimation = new ColorTween(begin: Colors.grey, end: Colors.red).animate(_controller)..addListener(() {setState((){});});
    textAnimation = new ColorTween(begin: Colors.grey[300], end: Colors.white).animate(_controller)..addListener(() {setState((){});});
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor, ),
        backgroundColor: Theme.of(context).canvasColor,
        brightness: Brightness.dark,
        title: new Text(_selectedClass != null ? _selectedClass.number : "", style: Theme.of(context).textTheme.title.copyWith(color: Theme.of(context).primaryColor),),
      ),
      body: new Container(
        child: Center(
            child: _listBuilder
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () async{
          if(_selectedClass != null){
            await bloc.addClassEvent(SetClass(_selectedClass));
            widget.onSelect();
          }
        },
        child: new Container(
          color: backgroundAnimation.value,
          height: kBottomNavigationBarHeight,
          child: Center(
            child: Text("Wybierz", style: Theme.of(context).textTheme.display1.copyWith(color: textAnimation.value,)),
          ),
        ),
      ),
    );
  }
}
