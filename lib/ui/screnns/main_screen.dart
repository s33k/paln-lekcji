import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';
import 'package:timetable/ui/screnns/perrmision_check_screen.dart';
import 'package:timetable/ui/screnns/timetable_choice_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  State createState() => new MainScreenState();
}

class MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: TimetableChoice());
  }

  void _checkPermission() async {
    bool permissionStatus = await SimplePermissions.checkPermission(
        Permission.WriteExternalStorage);
    if (!permissionStatus) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => PermissionCheckScreen()),
          (Route<dynamic> route) => false);
      return;
    }

    SharedPreferencesHelper preferencesHelper = new SharedPreferencesHelper();
    var timetableStatus = await preferencesHelper.getDownloadedTimetableConf();
    var classStatus = await preferencesHelper.getClass();

    int fourNull = 0;
    timetableStatus.forEach((element) => element == null ? fourNull++ : fourNull);

    if (fourNull == 4 && classStatus == null) {
      Navigator.of(context).pushNamedAndRemoveUntil('start', (Route<dynamic> route) => false);
    }
  }
}
