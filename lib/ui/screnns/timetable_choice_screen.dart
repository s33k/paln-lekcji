import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';

import 'package:timetable/ui/screnns/timetables/timetable_list_screen.dart';
import 'package:timetable/ui/screnns/timetables/timetable_table_screen.dart';
import 'package:timetable/ui/widgets/main_drawer.dart';
import 'package:timetable/ui/widgets/timetable_bottom_menu.dart';

class TimetableChoice extends StatefulWidget {
  @override
  State createState() => TimetableChoiceState();
}

class TimetableChoiceState extends State<TimetableChoice>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;
  ScheduleBloc scheduleBloc;
  SettingsBloc settingsBloc;
  bool showedOldTimetableDialog = false;
  bool settingsOpen = false;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    scheduleBloc = BlocProvider.of<ScheduleBloc>(context);
    settingsBloc = BlocProvider.of<SettingsBloc>(context);
    scheduleBloc.add(LoadSchedule());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: StreamBuilder(
            stream: scheduleBloc.state,
            builder: (context, AsyncSnapshot<ScheduleModel> snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                settingsBloc.state.first.then((settings) {
                  if ((settings.selectedClass.number !=
                              snapshot.data.actuallyClass.number ||
                          settings.url != snapshot.data.actuallyUrl) &&
                      !settingsOpen) {
                    showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Nieaktualny plan"),
                            content: Text(
                                "Obecny plan jest dla innej klasy. Chcesz pobrać plan dla wybranej obecnie klasy?"),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text("Anuluj"),
                              ),
                              FlatButton(
                                onPressed: () async {
                                  await scheduleBloc.add(DownloadSchedule());
                                  Navigator.of(context).pop();
                                },
                                child: Text("Pobierz"),
                              )
                            ],
                          );
                        });
                    showedOldTimetableDialog = true;
                  }
                });
                return new TabBarView(
                  controller: _tabController,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    TimetableListScreen(
                      timetable: snapshot.data.schedule,
                    ),
                    TimetableTableScreen(
                      timetable: snapshot.data.schedule,
                    ),
                  ],
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.settings),
          backgroundColor: Theme.of(context).canvasColor,
          foregroundColor: Theme.of(context).primaryColor,
          elevation: 3.0,
          onPressed: _showBottomMenu,
        ),
        bottomNavigationBar: TabBar(
          controller: _tabController,
          indicator: BoxDecoration(
              border: Border(
                  top: BorderSide(
                      width: 2.5, color: Theme.of(context).primaryColor))),
          indicatorWeight: 5.0,
          tabs: <Widget>[
            new Tab(
              child: Icon(
                Icons.list,
                color: Theme.of(context).primaryColor,
              ),
            ),
            new Tab(
              child: Icon(
                Icons.apps,
                color: Theme.of(context).primaryColor,
              ),
            )
          ],
        ),
    );
  }

  void _showBottomMenu() {
    TimetableBottomMenu bottomMenu = new TimetableBottomMenu(onSettingsOpen: () => settingsOpen = true, onSettingsClose: () => settingsOpen = false);

    var controller = showBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return bottomMenu;
        });

    bottomMenu.setOnClose(controller.close);
  }
}
