import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';

class PermissionCheckScreen extends StatefulWidget {
  @override
  State createState() => new PermissionCheckScreenState();
}

class PermissionCheckScreenState extends State<PermissionCheckScreen> {
  bool _permissionStatus;

  @override
  void initState() {
    super.initState();
    _checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Spacer(flex: 2,),
              Icon(
                Icons.folder,
                color: Theme.of(context).primaryColor,
                size: 216.0,
              ),
              Spacer(flex: 2,),
              Text(
                "Musisz pozwolić aplikacji na korzystanie z plików, bez tego aplikacja nie będzie działać.",
                style: Theme.of(context).textTheme.headline,
                textAlign: TextAlign.center,
              ),
              Spacer(),
              RaisedButton(
                onPressed: _requestPermission,
                color: Theme.of(context).primaryColor,
                child: Text(
                  "Pozwól",
                  style: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: Theme.of(context).canvasColor),
                ),
              ),
              Spacer(flex: 4,)
            ],
          ),
        ),
      ),
    );
  }

  void _checkPermission() async{
    _permissionStatus = await SimplePermissions.checkPermission(Permission.WriteExternalStorage);
    if(_permissionStatus) _redirect();
  }

  void _requestPermission() async{
    await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
    _checkPermission();
  }

  void _redirect(){
    Navigator.of(context).pushNamedAndRemoveUntil("start", (Route<dynamic> route) => false);
  }
}
