import 'package:flutter/material.dart';
import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/code/schedule.dart';
import 'package:timetable/ui/screnns/timetables/auto_select_hour_state.dart';
import 'package:timetable/ui/widgets/lessons_table.dart';

class TimetableTableScreen extends TimetableScreen {
  const TimetableTableScreen({Key key, Schedule timetable})
      : super(key: key, timetable: timetable);

  @override
  State createState() => new TimetableTableScreenState();
}

class TimetableTableScreenState extends AutoHourState
    with AutomaticKeepAliveClientMixin {
  double _containerHeight = 100;
  Map<int, double> _heights = {};
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    if(DateTime.now().weekday > 5){
      timer.cancel();
      actuallyHour = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    hours = widget.timetable.allHours;

    _calculateHeights();

    return new Container(
      padding: EdgeInsets.only(top: kToolbarHeight / 2),
      child: ListView(padding: EdgeInsets.all(0.0), children: <Widget>[
        new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Expanded(
                flex: 5,
                child: new Container(
                  constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height -
                          kBottomNavigationBarHeight * 1.41),
                  height: _containerHeight,
                  decoration: BoxDecoration(
                    color: Theme.of(context).canvasColor,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Theme.of(context).backgroundColor,
                          offset: Offset(5.0, 0.0),
                          blurRadius: 10.0)
                    ],
                  ),
                  child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      itemCount: hours.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        Hour hour = index - 1 >= 0
                            ? hours[index - 1]
                            : Hour(0, Duration(), Duration());
                        return InkWell(
                          onTap: () {
                            if (hour.number != 0) {
                              setState(() {
                                actuallyHour = actuallyHour == hour
                                    ? actuallyHour = null
                                    : actuallyHour = hour;
                                hourClicked = true;
                              });
                            }
                          },
                          child: Container(
                            height: index == 0 ? 50.0 : _heights[index],
                            margin: EdgeInsets.all(0),
                            padding: EdgeInsets.all(0),
                            decoration: BoxDecoration(
                                color: index != 0
                                    ? hours[index - 1] != actuallyHour
                                        ? Theme.of(context).canvasColor
                                        : Theme.of(context).primaryColorLight
                                    : Theme.of(context).canvasColor,
                                border: Border.all(
                                    color: Theme.of(context).canvasColor,
                                    width: 0.0)),
                            child: Column(
                              children: <Widget>[
                                index > 1
                                    ? Divider(
                                        height: 0.0,
                                        color: Theme.of(context).dividerColor,
                                      )
                                    : Container(),
                                Padding(
                                  padding: const EdgeInsets.only(left: 4.0),
                                  child: index != 0
                                      ? Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(hour.number.toString() + ". "),
                                            Text(
                                              hour.getStartText() +
                                                  '\n' +
                                                  hour.getEndText(),
                                              textAlign: TextAlign.center,
                                            )
                                          ],
                                        )
                                      : new Text(""),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                )),
            new Expanded(
                flex: 20,
                child: NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overScroll) {
                    overScroll.disallowGlow();
                  },
                  child: new SingleChildScrollView(
                      controller: _scrollController,
                      scrollDirection: Axis.horizontal,
                      child: new LessonsTable(
                        timetable: widget.timetable,
                        containerHeight: _containerHeight,
                        heights: _heights,
                        hour: actuallyHour,
                      )),
                )),
          ],
        ),
      ]),
    );
  }

  void _calculateHeights() {
    List<dynamic> hours = widget.timetable.allHours;
    List<Day> days = List.from(widget.timetable.allDays.values);
    var heights = List.generate(hours.length, (_) => new List(5));

    for (int hourNumber = 0; hourNumber < hours.length; hourNumber++) {
      for (int dayNumber = 0; dayNumber < 5; dayNumber++) {
        List<Lesson> lessonsThatHour = days[dayNumber].lessons;
        var lessons = lessonsThatHour.where((lesson) {
          if (lesson.hour.number == hours[hourNumber].number) return true;
          return false;
        });
        int lessonsCount = lessons.length;
        if (lessonsCount > 1) {
          heights[hourNumber][dayNumber] = 37.5 * lessonsCount;
        } else {
          heights[hourNumber][dayNumber] = 75.0;
        }
      }

      double height = 75;
      heights[hourNumber].forEach((value) {
        if (value > height) height = value;
      });
      _heights[hours[hourNumber].number] = height;
    }
    _containerHeight = 50;
    _heights.values.forEach((value) => _containerHeight += value);
  }

  @override
  bool get wantKeepAlive => true;
}
