import 'package:flutter/material.dart';
import 'package:timetable/src/code/schedule.dart';
import 'package:timetable/ui/screnns/timetables/auto_select_hour_state.dart';
import 'package:timetable/ui/widgets/lessons_list.dart';

class TimetableListScreen extends TimetableScreen{

  const TimetableListScreen({Key key, Schedule timetable}) : super(key: key, timetable: timetable);

  @override
  State createState() => TimetableListScreenState();
}

class TimetableListScreenState extends AutoHourState with SingleTickerProviderStateMixin{

  TabController _tabController;
  int actuallyDay;
  List<String> _days = [
    'Poniedziałek',
    'Wtorek',
    'Środa',
    'Czwartek',
    'Piątek'
  ];

  @override
  void initState() {
    super.initState();
    actuallyDay = DateTime.now().weekday - 1 < 5 ? DateTime.now().weekday - 1 : null;
    _tabController = new TabController(vsync: this, length: 5, );
    if(actuallyDay != null){
      _tabController.animateTo(actuallyDay);
    }
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight + 15.0),
          child: new Container(
            child: new SafeArea(
              child: Column(
                children: <Widget>[
                  Container(height: 15.0,),
                  TabBar(
                    controller: _tabController,
                    isScrollable: true,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: Theme.of(context).primaryColor,
                    tabs: _getTabs(),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: widget.timetable != null ? TabBarView(
          controller: _tabController,
          children: _getLessonsListsWidgets(),
        ) : Center(child: CircularProgressIndicator(),)
      );
    }

    List<Widget> _getTabs(){
      List<Widget> _widgets = [];

      for(int i = 0; i < 5; i++){
        _widgets.add(
          new Tab(
            child: Text(
              _days[i],
              style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor),
            ),
          )
        );
      }
      return _widgets;
    }

    List<Widget> _getLessonsListsWidgets(){
      List<Widget> listToReturn = [];

      widget.timetable.allDays.forEach((dayName, dayValue){
        bool enabled = widget.timetable.allDays.keys.toList().indexOf(dayName) == actuallyDay;
        listToReturn.add(LessonsList(lessons: dayValue.lessons, hour: enabled ? actuallyHour : null, hourEnabled: enabled,));
      });

      return listToReturn;
    }
}