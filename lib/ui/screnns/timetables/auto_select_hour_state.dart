import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/schedule.dart';

abstract class TimetableScreen extends StatefulWidget{
  final Schedule timetable;

  const TimetableScreen({Key key, this.timetable}) : super(key: key);
}

class AutoHourState extends State<TimetableScreen>{
  List<dynamic> hours;
  Hour actuallyHour;
  bool hourClicked = false;
  Timer timer;

  @override
  Widget build(BuildContext context) {
    return null;
  }

  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 5), (_){
      setState(() {
        _setActualHour();
      });
    });
    _setActualHour();
    super.initState();
  }

  void _setActualHour(){
    hours = widget.timetable.allHours;
    bool hourWasSet = false;
    hours.forEach((hour) {
      if (_checkActualHour(hour) && !hourWasSet) {
        actuallyHour = !hourClicked ? hour : actuallyHour;
        hourWasSet = true;
      }
    });
  }

  bool _checkActualHour(Hour hour) {
    var time = new Duration(
        hours: DateTime.now().hour, minutes: DateTime.now().minute);
    if (time > hour.end) {
      return false;
    } else if (time > hour.start) {
      return true;
    } else {
      return true;
    }
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }
}