import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';
import 'package:timetable/src/helpers/download_helper.dart';
import 'package:timetable/ui/screnns/select_class_screen.dart';

class StartScreen extends StatefulWidget {
  @override
  State createState() => new StartScreenState();
}

class StartScreenState extends State<StartScreen>
    with SingleTickerProviderStateMixin {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  SettingsBloc bloc;
  ScheduleBloc scheduleBloc;
  TextEditingController _controller;

  @override
  void initState() {
    bloc = BlocProvider.of<SettingsBloc>(context);
    scheduleBloc = BlocProvider.of<ScheduleBloc>(context);
    _controller = new TextEditingController();
    _controller.addListener((){
      bloc.addUrlValidatorEvent(CheckUrl(_controller.value.text));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Container(
      alignment: Alignment.center,
      child: StreamBuilder<Object>(
        stream: null,
        builder: (context, snapshot) {
          return Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(flex: 4,),
                  Text("Wpisz link do planu", style: Theme.of(context).textTheme.display1,),
                  Spacer(),
                  TextFormField(
                    controller: _controller,
                    autofocus: true,
                    autovalidate: true,
                    validator: (value) {
                      if(snapshot.data == "" || snapshot.data == null || snapshot.data == true){

                      }else{
                        return snapshot.data;
                      }
                    },
                    onSaved: (value) {
                      bloc.addUrlValidatorEvent(SubmitUrl(value));
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SelectClass(
                                      onSelect: () async{
                                        scheduleBloc.add(DownloadSchedule());
                                        Navigator.of(context).pushNamedAndRemoveUntil(
                                            "timetable", (Route<dynamic> route) => false);
                                      },
                                    )));
                          }
                        },
                        color: Theme.of(context).primaryColor,
                        child: Text("Dalej", style: Theme.of(context).textTheme.button.copyWith(color: Theme.of(context).canvasColor),),
                      ),
                    ],
                  ),
                  Spacer(flex: 6,)
                ],
              ),
            ),
          );
        }
      ),
    ));
  }
}
