import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_page_indicator/flutter_page_indicator.dart';
import 'package:timetable/src/blocs/my%20lessons/my_lessons_bloc.dart';
import 'package:timetable/src/blocs/my%20lessons/my_lessons_events.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/ui/widgets/select_lesson_widget.dart';

class SelectMyLessonsScreen extends StatefulWidget {
  @override
  _SelectMyLessonsScreenState createState() => _SelectMyLessonsScreenState();
}

class _SelectMyLessonsScreenState extends State<SelectMyLessonsScreen> {
  ScheduleBloc scheduleBloc;
  MyLessonsBloc myLessonsBloc;
  bool _finish = false;

  @override
  void initState() {
    scheduleBloc = BlocProvider.of<ScheduleBloc>(context);
    myLessonsBloc = BlocProvider.of<MyLessonsBloc>(context);
    myLessonsBloc.add(LoadLessonsToSelect());
    super.initState();
  }

  Lesson _selectedLesson(List<Lesson> lessons) {
    if (lessons.every((lesson) => lesson.show)) {
      return null;
    } else if (lessons.every((lesson) => !lesson.show)) {
      return Lesson('', '', '', Hour(0, Duration(), Duration()), '', '', false);
    }else{
      var activeLessons = lessons.where((lesson) => lesson.show);
      if(activeLessons.length != 1){
        return null;
      }else{
        return activeLessons.first;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Theme.of(context).canvasColor,
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        title: Text(
          "Wybierz lekcje na które chodzisz",
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: StreamBuilder(
        stream: myLessonsBloc.stream,
        // ignore: missing_return
        builder: (context, AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.done:
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
              break;
            case ConnectionState.active:
              SwiperController controller = new SwiperController();
              SwiperCustomPagination pagination = new SwiperCustomPagination(
                  builder: (BuildContext context, SwiperPluginConfig config) {
                return Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        margin: EdgeInsets.all(10),
                        child: FractionPaginationBuilder(
                                color: Theme.of(context).hintColor)
                            .build(context, config)));
              });
              return Swiper(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  Lesson select = _selectedLesson(snapshot.data[index]['lessons']);
                  return Container(
                    child: Center(
                        child: SelectLessonCard(
                      day: snapshot.data[index]['day'],
                      hour: snapshot.data[index]['hour'],
                      lessons: snapshot.data[index]['lessons'],
                      select: select,
                      onSelect: (result) {
                        myLessonsBloc.add(SelectLesson(
                            selectedLesson: result,
                            lessons: snapshot.data[index]['lessons']));
                        if(index == snapshot.data.length - 1) _finish = true;
                        controller.next();
                      },
                    )),
                  );
                },
                pagination: pagination,
                controller: controller,
                loop: false,
                onIndexChanged: (index){
                  if(index == 0 && _finish){
                    _finish = false;
                    showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context){
                          return AlertDialog(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7.5)
                            ),
                            title: Text("Wybrałeś wszystkie lekcje"),
                            content: Text("Co chcesz teraz zrobić?"),
                            actions: <Widget>[
                              FlatButton(child: Text("Zostaje tutaj"), onPressed: () => Navigator.of(context).pop(),),
                              FlatButton(child: Text("Wracam do planu"), onPressed: (){
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              },)
                            ],
                          );
                        }
                    );
                  }
                },
                indicatorLayout: PageIndicatorLayout.WARM,
              );
          }
        },
      ),
    );
  }
}
