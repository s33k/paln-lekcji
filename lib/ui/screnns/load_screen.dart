import 'dart:async';

import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:timetable/src/blocs/theme/bloc.dart';

class LoadScreen extends StatefulWidget {
  @override
  _LoadScreenState createState() => _LoadScreenState();
}

class _LoadScreenState extends State<LoadScreen> {
  bool _themeLoaded = false;

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      child: Center(
        child: Icon(
          Icons.event_note,
          color: Colors.white,
          size: 128.0,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> _loadTheme() async{
    while(!_themeLoaded){
      _themeLoaded = await BlocProvider.of<ThemeBloc>(context).themeStream.first != null;
    }
    return true;
  }

  void _load() async{
    bool _themeReady = await _loadTheme();

    if(_themeReady){
      Navigator.of(context).pushReplacementNamed('main');
    }
  }
}
