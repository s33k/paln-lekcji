import 'package:flutter/material.dart';
import 'package:timetable/ui/widgets/class_tile.dart';
import 'package:timetable/ui/widgets/link_tile_widget.dart';

import 'package:timetable/ui/widgets/theme_widget.dart';

class SettingsScreen extends StatefulWidget{

  @override
  SettingsScreenState createState() {
    return new SettingsScreenState();
  }
}

class SettingsScreenState extends State<SettingsScreen> with AutomaticKeepAliveClientMixin{
  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Ustawienia", style: Theme.of(context).textTheme.title.copyWith(color: Theme.of(context).primaryColor),),
          backgroundColor: Theme.of(context).canvasColor,
          iconTheme: Theme.of(context).primaryIconTheme.copyWith(color: Theme.of(context).primaryColor),
          elevation: 1.0,
          brightness: Brightness.dark,
        ),
        body: new Container(
          child: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overScroll){
              overScroll.disallowGlow();
            },
            child: new ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only( bottom: 40.0, left: 15.0),
//                  child: new Text("Ustawienia", style: Theme.of(context).textTheme.display2.copyWith(color: Theme.of(context).primaryColor),),
                ),
                _sectionHeader(context, "Wygląd", marginTop: 0.0, icon: Icon(Icons.palette, color: Theme.of(context).primaryColor,)),
                ThemeTile(),
                _sectionHeader(context, "Plan", icon: Icon(Icons.edit, color: Theme.of(context).primaryColor,)),
                LinkTile(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                ),
                ClassTile()
              ],
            ),
          ),
        ),
      );
    }

    Widget _sectionHeader(BuildContext context, String text, {double marginTop, Widget icon}){
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        margin: EdgeInsets.only(top: marginTop != null ? marginTop : 40.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                icon != null ? icon : null,
                Text(text, style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor, fontWeight: FontWeight.w300)),
              ],
            ),
            Container(
             decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(Radius.circular(360))
              ),
              height: 1.0,
              margin: EdgeInsets.symmetric(vertical: 5.0),
            ),
          ],
        ),
      );
    }

  @override
  bool get wantKeepAlive => true;
}