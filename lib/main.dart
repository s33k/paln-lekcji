import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:timetable/src/blocs/my%20lessons/my_lessons_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/settings/settings_bloc.dart';
import 'package:timetable/src/blocs/theme/bloc.dart';
import 'package:timetable/ui/screnns/select_my_lessons_screen.dart';
import 'package:timetable/ui/screnns/settings_screen.dart';
import 'package:timetable/ui/screnns/start_screen.dart';
import 'package:bloc_provider/bloc_provider.dart';
import 'package:timetable/ui/screnns/main_screen.dart';

void main() async {
  runApp(LaunchApp());
}

class LaunchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return BlocProviderTree(
      blocProviders: [
        BlocProvider<ThemeBloc>(creator: (_context, _bag) => ThemeBloc()),
        BlocProvider<SettingsBloc>(creator: (_context, _bag) => SettingsBloc()),
        BlocProvider<ScheduleBloc>(creator: (_context, _bag) => ScheduleBloc()),
        BlocProvider<MyLessonsBloc>(creator: (_context, _bag) => MyLessonsBloc())
      ],
      child: StartApp(),
    );
  }
}

class StartApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ThemeBloc themeBloc = BlocProvider.of<ThemeBloc>(context);

    return StreamBuilder(
      initialData: themeBloc.initialData,
      stream: themeBloc.themeStream,
      builder: (context, snapshot) {
        ThemeModel themeModel = snapshot.data;
        return MaterialApp(
          title: 'Flutter Demo',
          theme: themeModel.themeData,
          routes: <String, WidgetBuilder>{
            'timetable': (BuildContext context) => MainScreen(),
            'start': (BuildContext context) => StartScreen(),
            'settings': (BuildContext context) => SettingsScreen(),
            'select my lessons': (BuildContext context) => SelectMyLessonsScreen(),
          },
          home: MainScreen(),
        );
      }
    );
  }
}
