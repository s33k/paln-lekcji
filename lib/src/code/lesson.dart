import 'package:timetable/src/code/hour.dart';

class Lesson{
  String _room;
  String _teacher;
  String _subject;
  String _groupNumber;
  String _day;
  bool show;
  Hour hour;
  int id;

  Lesson(this._subject, this._teacher, this._room, this.hour, this._groupNumber, this._day, this.show);

  Lesson copyWith({String subject, String teacher, String room, Hour hour, String groupNumber, String day, bool show}){
    Lesson lesson = new Lesson(
      subject ?? _subject,
      teacher ?? _teacher,
      room ?? _room,
      hour ?? this.hour,
      groupNumber ?? _groupNumber,
      day ?? _day,
      show ?? this.show
    )..id = this.id;
    return lesson;
  }

  Lesson.fromMap(Map<String, dynamic> map){
    _room = map['room'] ?? "";
    _teacher = map['teacher'] ?? "";
    _subject = map['subject'];
    _groupNumber = map['group_number'] ?? "";
    _day = map['day'];
    show = map['show'] == 1 ? true : false;
    hour = new Hour(map['hour'], new Duration(), new Duration());
    id = map['id'];
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'room': _room,
      'teacher': _teacher,
      'subject': _subject,
      'group_number': _groupNumber,
      'day': _day,
      'show': show,
      'hour': hour.number,
      'id': id
    };
    return map;
  }

  //getter and setters
  String get room => _room;
  String get teacher => _teacher;
  String get subject => _subject;
  String get group => _groupNumber;
  String get day => _day;
}

enum LessonStatus{
  PAST,
  NOW,
  FUTURE
}