import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';

class Day{
  List<Lesson> lessons;
  Days _day;

  Day(this.lessons, this._day);

  //getter and setters
  Days get day => _day;

  int hoursCount(){
    List<int> hours = [];

    lessons.forEach((lesson){
      if(hours.indexOf(lesson.hour.number) >= 0){
      }else{
        hours.add(lesson.hour.number);
      }
    });

    return hours.length;
  }

  Hour start(){
    return lessons.first.hour;
  }

  Hour end(){
    return lessons.last.hour;
  }

}

class DayNames{

  Map<String, String> _polishEnglish = {
    "Poniedziałek": "MONDAY",
    "Wtorek": "TUESDAY",
    "Środa": "WEDNESDAY",
    "Czwartek": "THURSDAY",
    "Piątek": "FRIDAY"
  };

  Map<String, String> _englishPolish = {
    "MONDAY": "Poniedziałek",
    "TUESDAY": "Wtorek",
    "WEDNESDAY": "Środa",
    "THURSDAY": "Czwartek",
    "FRIDAY": "Piątek"
  };

  Map<Days, String> _enumPolish = {
    Days.MONDAY: "Poniedziłek",
    Days.TUESDAY: "Wtorek",
    Days.WEDNESDAY: "Środa",
    Days.THURSDAY: "Czwartek",
    Days.FRIDAY: "Piątek"
  };

  Map<Days, String> _enumEnglish = {
    Days.MONDAY: "MONDAY",
    Days.TUESDAY: "TUESDAY",
    Days.WEDNESDAY: "THURSDAY",
    Days.THURSDAY: "THURSDAY",
    Days.FRIDAY: "FRIDAY"
  };

  List<String> _polish = [
    "Poniedziałek",
    "Wtorek",
    "Środa",
    "Czwartek",
    "Piątek"
  ];

  List<String> _english = [
    "MONDAY",
    "TUESDAY",
    "WEDNESDAY",
    "THURSDAY",
    "FRIDAY"
  ];

  List<String> get polish => _polish;
  List<String> get english => _english;
  Map<String, String> get englishPolish => _englishPolish;
  Map<String, String> get polishEnglish => _polishEnglish;
  Map<Days, String> get enumPolish => _enumPolish;
}

enum Days{
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY
}