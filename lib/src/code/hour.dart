class Hour{
  int number;
  Duration start, end;

  Hour(this.number, this.start, this.end);

  Hour.fromMap(Map<String, dynamic> map) {
    number = map['id'];
    start = new Duration(hours: int.parse(map['start'].substring(0, map['start'].lastIndexOf(':'))), minutes: int.parse(map['start'].substring(map['start'].lastIndexOf(':') + 1)));
    end = new Duration(hours: int.parse(map['end'].substring(0, map['end'].lastIndexOf(':'))), minutes: int.parse(map['end'].substring(map['end'].lastIndexOf(':') + 1)));
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'id': number,
      'start': start.toString().substring(0, start.toString().lastIndexOf(':')),
      'end': end.toString().substring(0, end.toString().lastIndexOf(':'))
    };
    return map;
  }

  String getText(){
    return number.toString() + '. ' +
        start.toString().substring(0, start.toString().lastIndexOf(':'))
        +
        ' - '
        +
        end.toString().substring(0, end.toString().lastIndexOf(':'));
  }

  String getStartText(){
    return start.toString().substring(0, start.toString().lastIndexOf(':'));
  }

  String getEndText(){
    return end.toString().substring(0, end.toString().lastIndexOf(':'));
  }
}