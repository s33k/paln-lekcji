import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/code/lesson.dart';

class Schedule {
  Map<String, Day> _days = {
    "MONDAY": new Day(new List<Lesson>(), Days.MONDAY),
    "TUESDAY": new Day(new List<Lesson>(), Days.TUESDAY),
    "WEDNESDAY": new Day(new List<Lesson>(), Days.WEDNESDAY),
    "THURSDAY": new Day(new List<Lesson>(), Days.THURSDAY),
    "FRIDAY": new Day(new List<Lesson>(), Days.FRIDAY),
  };
  List<dynamic> _hours;

  //getter and setters
  Map<String, Day> get allDays => _days;
  List<dynamic> get allHours => _hours;

  Schedule(List<dynamic> lessons, List<dynamic> hours) {
    this._hours = hours;
    for(Lesson lesson in lessons){
      lesson.hour = hours[lesson.hour.number - 1];
      switch(lesson.day){
        case "MONDAY":
          _days['MONDAY'].lessons.add(lesson);
          break;
        case "TUESDAY":
          _days['TUESDAY'].lessons.add(lesson);
          break;
        case "WEDNESDAY":
          _days['WEDNESDAY'].lessons.add(lesson);
          break;
        case "THURSDAY":
          _days['THURSDAY'].lessons.add(lesson);
          break;
        case "FRIDAY":
          _days['FRIDAY'].lessons.add(lesson);
          break;
      }
    }
    _days.forEach((dayName, value){
      value.lessons.sort((a, b) => a.hour.number.compareTo(b.hour.number));
    });
  }
}

