class Class{
  String number;
  String path;

  Class({this.number, this.path});

  Class.fromMap(Map<String, dynamic> map){
    this.number = map['number'];
    this.path = map['path'];
  }

  Class.fromList(List<String> list){
    this.number = list[0] ?? null;
    this.path = list[1] ?? null;
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'number': number,
      'path': path
    };
    return map;
  }
}

