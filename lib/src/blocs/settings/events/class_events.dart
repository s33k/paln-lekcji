import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:timetable/src/code/class.dart';

@immutable
abstract class SettingsClassEvent extends Equatable{
  SettingsClassEvent([List props = const []]) : super(props);
}

class LoadClassList extends SettingsClassEvent{

}

class SetClass extends SettingsClassEvent{
  final Class selectedClass;

  SetClass(this.selectedClass);
}