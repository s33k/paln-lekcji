import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class SettingsUrlValidatorEvent extends Equatable{
  SettingsUrlValidatorEvent([List props = const []]) : super(props);
}

class CheckUrl extends SettingsUrlValidatorEvent{
  final String url;

  CheckUrl(this.url);
}

class SubmitUrl extends SettingsUrlValidatorEvent{
  final String url;

  SubmitUrl(this.url);
}