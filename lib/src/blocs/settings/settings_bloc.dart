import 'dart:async';
import 'dart:io';

import 'package:timetable/src/helpers/download_helper.dart';
import 'package:validators/validators.dart';
import 'package:bloc_provider/bloc_provider.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timetable/src/blocs/settings/events/class_events.dart';
import 'package:timetable/src/blocs/settings/events/url_validator_events.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';

class SettingsBloc extends Bloc {
  PublishSubject _urlValidator = new PublishSubject();
  PublishSubject _classList = new PublishSubject();
  BehaviorSubject<SettingsModel> _settingsSubject =
      new BehaviorSubject<SettingsModel>();

  Stream<SettingsModel> get state => _settingsSubject.stream;

  Stream get urlValidatorState => _urlValidator.stream;

  Stream get classListState => _classList.stream;

  PublishSubject _urlValidatorController = new PublishSubject();
  PublishSubject _classListController = new PublishSubject();

  Function(SettingsUrlValidatorEvent) get addUrlValidatorEvent =>
      _urlValidatorController.add;

  Function(SettingsClassEvent) get addClassEvent => _classListController.add;

  SettingsModel _settingsModel;

  SettingsBloc() {
    _loadSettings();
    _urlValidatorController.listen(_onUrlValidatorEvent);
    _classListController.listen(_onClassEvent);
  }

  _onUrlValidatorEvent(dynamic event) async{
    if (event is CheckUrl) {
      if (event.url.isEmpty) {
        _urlValidator.sink.add("Nie wprowadzono linku");
      } else if (!isURL(event.url)) {
        _urlValidator.sink.add("Wprowadź poprawy link");
      } else {
        _urlValidator.sink.add(true);
      }
    } else if (event is SubmitUrl) {
      String value = event.url;

      if (value.startsWith("https://") || value.startsWith("http://")) {
      } else {
        value = "http://" + value;
      }

      if (value.endsWith("/")) value = value.substring(0, value.lastIndexOf("/"));

      _settingsModel = SettingsModel(url: value, selectedClass: null);

      SharedPreferencesHelper().setLinkToTimetable(value);
      SharedPreferencesHelper().deleteClass();
      await _settingsSubject.sink.add(_settingsModel);
    } else {
      throw Exception("Wrong event. You must send SettingsUrlValidatorEvent!");
    }
  }

  _onClassEvent(dynamic event) async {
    if (event is LoadClassList) {
      List<Class> _classes = await DownloadHelper().downloadClasses().catchError((error) {
        if(error is SocketException){
          _classList.sink.add("Problem z połączeniem :( \n\n * Sprawdź czy podałeś prawidłowy link \n * Sprawdź połączenie z internetem");
        }
      });
      if (_classes != null) {
        _classList.sink.add(_classes);
      }
    } else if (event is SetClass) {
      Class _class = event.selectedClass;
      _settingsModel = _settingsModel.copyWith(newSelectedClass: _class);

      SharedPreferencesHelper().setClass(_class);
      _settingsSubject.sink.add(_settingsModel);
    } else {
      throw Exception("Wrong event. You must send SettingsClassEvent!");
    }
  }

  _loadSettings() async {
    SharedPreferencesHelper _preferences = new SharedPreferencesHelper();
    Class _class = await _preferences.getClass();
    String _url = await _preferences.getLinkToTimetable();

    _settingsModel = new SettingsModel(url: _url, selectedClass: _class);
    _settingsSubject.sink.add(_settingsModel);
  }

  @override
  void dispose() {
    _settingsSubject.close();
    _urlValidator.close();
    _classList.close();

    _urlValidatorController.close();
    _classListController.close();
  }
}

class SettingsModel {
  final String url;
  final Class selectedClass;

  SettingsModel({@required this.url, @required this.selectedClass});

  SettingsModel copyWith({String newUrl, Class newSelectedClass}) {
    return SettingsModel(
      url: newUrl ?? this.url,
      selectedClass: newSelectedClass ?? this.selectedClass,
    );
  }
}
