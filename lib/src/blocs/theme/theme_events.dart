import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:timetable/src/blocs/theme/theme_enum.dart';

@immutable
abstract class ThemeEvent extends Equatable{
  ThemeEvent([List props = const []]) : super(props);
}

class LoadThemeEvent extends ThemeEvent{

}

class ChangeThemeEvent extends ThemeEvent{
  final Themes theme;

  ChangeThemeEvent(this.theme) : super([theme]);
}