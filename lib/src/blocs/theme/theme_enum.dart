enum Themes{
  DEFAULT,
  DARK,
  AMOLED
}

final Map<Themes, String> themesNames = {
  Themes.DEFAULT: "Default",
  Themes.DARK: "Dark",
  Themes.AMOLED: "AMOLED",
};