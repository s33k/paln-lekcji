import 'dart:async';

import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timetable/src/blocs/theme/theme_enum.dart';
import 'package:timetable/src/blocs/theme/theme_events.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';

class ThemeBloc extends Bloc{

  static final Map<Themes, ThemeData> _themesMap = {
    Themes.DEFAULT: ThemeData(
        primarySwatch: Colors.red,
        cursorColor: Colors.red,
        dividerColor: Color.fromARGB(50, 0, 0, 0),
        backgroundColor: Color.fromARGB(25, 0, 0, 0),
        brightness: Brightness.light
    ),
    Themes.DARK: ThemeData(
        primaryColor: Colors.red,
        accentColor: Colors.red,
        cursorColor: Colors.red,
        toggleableActiveColor: Colors.red,
        textSelectionHandleColor: Colors.red,
        dividerColor: Color.fromARGB(75, 255, 255, 255),
        backgroundColor: Colors.grey[900],
        brightness: Brightness.dark,
        primaryColorLight: Colors.grey[700],
        bottomAppBarColor: Color.fromARGB(255, 40, 40, 40),
    ),
    Themes.AMOLED : ThemeData(
        primaryColor: Color.fromARGB(255, 200, 200, 200),
        accentColor: Colors.red,
        canvasColor: Colors.black,
        cursorColor: Colors.red,
        toggleableActiveColor: Colors.red,
        textSelectionHandleColor: Colors.red,
        dividerColor: Color.fromARGB(50, 255, 255, 255),
        cardColor: Colors.grey[900],
        backgroundColor: Colors.grey[900],
        brightness: Brightness.dark,
        primaryColorLight: Colors.grey[800],
        bottomAppBarColor: Colors.black,
    )
  };

  BehaviorSubject<ThemeModel> _themeSubject = new BehaviorSubject<ThemeModel>();
  Stream get themeStream => _themeSubject.stream;

  StreamController _themeController = new StreamController();
  Function(ThemeEvent) get add => _themeController.sink.add;

  ThemeModel get initialData => ThemeModel(theme: Themes.DEFAULT, themeData: _themesMap[Themes.DEFAULT]);

  ThemeBloc(){
    _themeController.stream.listen(_onEventThemeController);
    add(LoadThemeEvent());
  }

  void _onEventThemeController(dynamic event) async{
    if(event is LoadThemeEvent){
      String _themeName = await SharedPreferencesHelper().getTheme();

      Map<String, Themes> _stringToThemeMap = themesNames.map((key, value) => MapEntry(value, key));
      Themes _selectedTheme = _stringToThemeMap[_themeName];

      if(_selectedTheme == null){
        _selectedTheme = _stringToThemeMap.values.first;
      }

      ThemeData _themeData = _themesMap[_selectedTheme];

      _themeSubject.add(ThemeModel(theme: _selectedTheme, themeData: _themeData));
    }else if(event is ChangeThemeEvent){
      Themes _selectedTheme = event.theme;
      ThemeData _themeData = _themesMap[_selectedTheme];
      String _themeName = themesNames[_selectedTheme];

      _themeSubject.add(ThemeModel(theme: _selectedTheme, themeData: _themeData));
      SharedPreferencesHelper().setTheme(_themeName);
    }
  }

  @override
  void dispose(){
    _themeSubject.close();
    _themeController.close();
  }
}

class ThemeModel{
  final Themes theme;
  final ThemeData themeData;

  ThemeModel({@required this.theme, @required this.themeData});
}