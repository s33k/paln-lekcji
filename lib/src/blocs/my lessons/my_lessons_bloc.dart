import 'package:bloc_provider/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timetable/src/blocs/my%20lessons/my_lessons_events.dart';
import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/helpers/database_helper.dart';

class MyLessonsBloc extends Bloc{

  BehaviorSubject<List<Map<String, dynamic>>> _lessonsToSelectSubject = new BehaviorSubject<List<Map<String, dynamic>>>();
  PublishSubject<MyLessonsEvent> _eventSubject = new PublishSubject<MyLessonsEvent>();

  Stream<List<Map<String, dynamic>>> get stream => _lessonsToSelectSubject.stream;
  Function(MyLessonsEvent) get add => _eventSubject.sink.add;

  DBHelper _dbHelper;

  void eventHandler(MyLessonsEvent event) async {
    if(event is LoadLessonsToSelect){
      var results = await _dbHelper.getLessonsToSelect();
      List<Map<String, dynamic>> lessons = _sortResults(results);
      _lessonsToSelectSubject.sink.add(lessons);
    }else if(event is SelectLesson){
      event.lessons.forEach((element){
        if(event.selectedLesson != null && element.id == event.selectedLesson.id){
          element.show = true;
        }else{
          element.show = false;
        }
        _dbHelper.updateLesson(element);
      });
    }
  }

  List<Map<String, dynamic>> _sortResults(List<Map<String, dynamic>> lessons){
    DayNames dayNames = new DayNames();
    lessons.sort((a, b){
      if(a['day'] == b['day']){
        return a['hour'].number.compareTo(b['hour'].number);
      }else if(dayNames.english.indexOf(a['day']) > dayNames.english.indexOf(b['day'])){
        return 1;
      }else{
        return -1;
      }
    });
    return lessons;
  }

  MyLessonsBloc(){
    _dbHelper = DBHelper();
    _eventSubject.listen(eventHandler);
  }

  @override
  void dispose() {
    _lessonsToSelectSubject.close();
    _eventSubject.close();
  }
}