import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:timetable/src/code/lesson.dart';

@immutable
abstract class MyLessonsEvent extends Equatable{
  MyLessonsEvent({List props = const []}) : super(props);
}

class LoadLessonsToSelect extends MyLessonsEvent{}

class SelectLesson extends MyLessonsEvent{
  final Lesson selectedLesson;
  final List<Lesson> lessons;

  SelectLesson({@required this.selectedLesson, @required this.lessons});
}