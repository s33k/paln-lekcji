import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc_provider/bloc_provider.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/src/code/schedule.dart';
import 'package:timetable/src/helpers/database_helper.dart';
import 'package:timetable/src/helpers/download_helper.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';

class ScheduleBloc extends Bloc{

  DBHelper _dbHelper;

  PublishSubject _timetableLocalStatusSubject = new PublishSubject();
  BehaviorSubject<ScheduleModel> _timetableSubject = new BehaviorSubject<ScheduleModel>();
  BehaviorSubject<bool> _showLessonsStatusSubject = new BehaviorSubject<bool>.seeded(true);
  PublishSubject<ScheduleEvent> _timetableController = new PublishSubject();

  Stream<ScheduleModel> get state => _timetableSubject.stream;
  Stream<bool> get showLessonState => _showLessonsStatusSubject.stream;
  Stream get localStatusState => _timetableLocalStatusSubject.stream;
  Function(ScheduleEvent) get add => _timetableController.sink.add;

  ScheduleBloc(){
    _dbHelper = new DBHelper();
    _timetableController.listen(_eventHandler);
  }

  _loadSchedule() async{
    bool showOnlyMy = await SharedPreferencesHelper().getShowOnlyMyLessons();
    _showLessonsStatusSubject.sink.add(showOnlyMy);
    List<List> lessonsAndHours = await _dbHelper.getLessonsAndHours(showOnlyMyLessons: showOnlyMy);
    List lessons = lessonsAndHours[0];
    List hours = lessonsAndHours[1];

    List downloadedConfig = await SharedPreferencesHelper().getDownloadedTimetableConf();
    Class actuallyClass = downloadedConfig[0];
    String actuallyUrl = downloadedConfig[1];
    String generateDate = downloadedConfig[2];
    String beginDate = downloadedConfig[3];

    Schedule schedule = new Schedule(lessons, hours);

    ScheduleModel scheduleModel = new ScheduleModel(schedule: schedule, actuallyClass: actuallyClass, actuallyUrl: actuallyUrl, generateDate: generateDate, beginDate: beginDate);
    _timetableSubject.sink.add(scheduleModel);
  }

  _eventHandler(ScheduleEvent event) async{
    if(event is LoadSchedule){
      await _loadSchedule();
    }else if(event is ShowAllLessons){
      SharedPreferencesHelper().setShowMyLessons(false);
      _showLessonsStatusSubject.sink.add(false);

    }else if(event is ShowOnlyMyLessons){
      SharedPreferencesHelper().setShowMyLessons(true);
      _showLessonsStatusSubject.sink.add(true);

    }else if(event is AddLesson){
      await _dbHelper.insertLesson(event.lesson);

    }else if(event is EditLesson){
      await _dbHelper.updateLesson(event.lesson);

    }else if(event is DeleteLesson){
      await _dbHelper.deleteLesson(event.lesson.id);
      _loadSchedule();
    }else if(event is CheckScheduleStatus){
      bool result = await DownloadHelper().checkSchedule();
      _timetableLocalStatusSubject.sink.add(result);

    }else if(event is DownloadSchedule){
      DownloadHelper().downloadSchedule().then((_){
        _loadSchedule();
      });
    }
  }

  @override
  void dispose() {
    _timetableSubject.close();
    _timetableController.close();
    _timetableLocalStatusSubject.close();
    _showLessonsStatusSubject.close();
  }
}

class ScheduleModel{
  final Schedule schedule;
  final Class actuallyClass;
  final String actuallyUrl;
  final String generateDate;
  final String beginDate;

  ScheduleModel({@required this.schedule, @required this.actuallyClass, @required this.actuallyUrl, @required this.generateDate, @required this.beginDate});
}