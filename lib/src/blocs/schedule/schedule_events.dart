import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:timetable/src/code/lesson.dart';

@immutable
abstract class ScheduleEvent extends Equatable{
  ScheduleEvent({List props = const []}) : super(props);
}

class LoadSchedule extends ScheduleEvent{}

class ShowAllLessons extends ScheduleEvent{}

class ShowOnlyMyLessons extends ScheduleEvent{}

class AddLesson extends ScheduleEvent{
  final Lesson lesson;

  AddLesson(this.lesson);
}

class EditLesson extends ScheduleEvent{
  final Lesson lesson;

  EditLesson(this.lesson);
}

class DeleteLesson extends ScheduleEvent{
  final Lesson lesson;

  DeleteLesson(this.lesson);
}

class CheckScheduleStatus extends ScheduleEvent{}

class DownloadSchedule extends ScheduleEvent{}