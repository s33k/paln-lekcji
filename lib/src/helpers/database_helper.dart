import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper{

  static Database _db;
  static Map<String, dynamic> tablesClass = {'Lesson': Lesson, 'Hour': Hour, 'Class': Class};

  Future<Database> get db async{
    if(_db != null)
      return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async{
      io.Directory documentDirectory = await getApplicationDocumentsDirectory();
      String path = join(documentDirectory.path, 'database.db');
      var database = await openDatabase(path, version: 1, onCreate: _onCreate);
      return database;
  }

  void _onCreate(Database db, int version) async{
    await db.execute(
      "CREATE TABLE Lesson(id INTEGER PRIMARY KEY AUTOINCREMENT, subject TEXT, group_number TEXT, teacher TEXT, room TEXT, hour INTEGER, day TEXT, show INTEGER)");

    await db.execute(
      "CREATE TABLE Hour(id INTEGER PRIMARY KEY, start TEXT, end TEXT)");

    await db.execute(
      "CREATE TABLE Class(id INTEGER PRIMARY KEY, number TEXT, path TEXT)");
  }

  Future<List> _getElements(String tableName) async{
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM $tableName');
    List elements = new List();
    for(var map in list){
      switch(tablesClass[tableName]){
        case Lesson:
          elements.add(new Lesson.fromMap(map));
          break;
        case Hour:
          elements.add(new Hour.fromMap(map));
          break;
        case Class:
          elements.add(new Class.fromMap(map));
          break;
      }
    }
    return elements;
  }

  Future<List<List>> getLessonsAndHours({bool showOnlyMyLessons = false}) async{
    var dbClient = await db;

    List<Map> lessonsRaw;

    if(showOnlyMyLessons){
      lessonsRaw = await dbClient.rawQuery('SELECT * FROM Lesson WHERE show == 1');
    }else{
      lessonsRaw = await dbClient.rawQuery('SELECT * FROM Lesson');
    }

    List<Map> hoursRaw = await dbClient.rawQuery('SELECT * FROM Hour');
    List<dynamic> lessons = [], hours = [];

    for(var hour in hoursRaw){
      hours.add(new Hour.fromMap(hour));
    }

    for(var lesson in lessonsRaw){
      lessons.add(new Lesson.fromMap(lesson));
      //lessons.last.hour = hours[lessons.last.hour.number - 1];
    }
    return [lessons, hours];
  }

  _insertElement(String tableName, var element)async {
    var dbClient = await db;
    dbClient.insert(tableName, element.toMap());
  }

  _insertManyElements(String tableName, List<dynamic> elements)async {
    var dbClient = await db;
    for(var element in elements){
      dbClient.insert(tableName, element.toMap());
    }
  }

  Future<Hour> getHour(int id) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query('Hour',
        columns: ['id', 'start', 'end'],
        where: "id = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return new Hour.fromMap(maps.first);
    }
    return null;
  }

  Future<int> updateLesson(Lesson lesson) async {
    var dbClient = await db;
    return await dbClient.update("Lesson", lesson.toMap(),
        where: "id = ?", whereArgs: [lesson.id]);
  }

  Future<Class> getClass(String number) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query('Class',
        columns: ['id', 'number', 'path'],
        where: "number = ?",
        whereArgs: [number]);
    if (maps.length > 0) {
      return new Class.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Map<String, dynamic>>> getLessonsToSelect() async {
    var dbClient = await db;
    List<dynamic> result = await dbClient.rawQuery('SELECT * FROM Lesson WHERE group_number != \"\"');
    List<dynamic> hours = await getHours;

    List<Map<String, dynamic>> listToReturn = [];
    List<Lesson> lessons = [];
    result.forEach((element){
      lessons.add(Lesson.fromMap(element));
    });

    lessons.forEach((lesson){
      String day = lesson.day;
      int hourNumber = lesson.hour.number;
      bool lessonAdded = false;

      for(var element in listToReturn){
        if(element['day'] == day && element['hour'].number == hourNumber){
          element['lessons'].add(lesson);
          lessonAdded = true;
          break;
        }
      }
      if(!lessonAdded){
        listToReturn.add({
          'hour': hours[hourNumber - 1],
          'day': day,
          'lessons': [lesson]
        });
      }
    });
    return listToReturn;
  }

  _deleteRow(String tableName, int id) async{
    var dbClient = await db;
    dbClient.delete(tableName, where: "id = ?", whereArgs: [id]);
  }

  _deleteTable(String tableName) async{
    var dbClient = await db;
    dbClient.delete('$tableName');
  }

  Future<List<dynamic>> get getLessons => _getElements('Lesson');
  Future<List<dynamic>> get getHours => _getElements('Hour');
  Future<List<dynamic>> get getClasses => _getElements('Class');

  insertLesson(Lesson lesson) => _insertElement('Lesson', lesson);
  insertHour(Hour hour) => _insertElement('Hour', hour);
  insertClass(Class classObject) => _insertElement('Class', classObject);

  insertManyLessons(List<Lesson> lessons) => _insertManyElements('Lesson', lessons);
  insertManyHours(List<Hour> hours) => _insertManyElements('Hour', hours);
  insertManyClasses(List<Class> classes) => _insertManyElements('Class', classes );

  deleteLesson(int id) => _deleteRow("Lesson", id);

  deleteLessons() => _deleteTable('Lesson');
  deleteHours() => _deleteTable('Hour');
  deleteClasses() => _deleteTable('Class');
}