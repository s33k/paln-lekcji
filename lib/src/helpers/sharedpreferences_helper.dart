import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:timetable/src/code/class.dart';

class SharedPreferencesHelper{

  final String classKey = "Class";
  final String downloadedTimetableKey = "Downloaded timetable conf";
  final String showOnlyMyLessonsKey = "Show only my";
  final String timetableLinkKey = "Timetable link";
  final String themeKey = "Theme";

  /*
  SETTERS
   */
  void _setStringList({@required String name, @required List<String> data}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(name, data);
  }

  void _setString({@required String name, @required String data}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(name, data);
  }

  void _setBool({@required name, @required bool data}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(name, data);
  }

  void setClass(Class classToSave) async{
    _setStringList(name: classKey, data: [classToSave.number, classToSave.path]);
  }

  void setDownloadedTimetableConf(Class classToSave, String actuallyLinkToTimetable, {String generateDateText, String beginDateText}){
    _setStringList(name: downloadedTimetableKey, data: [classToSave.number, classToSave.path, actuallyLinkToTimetable, generateDateText, beginDateText]);
  }

  void deleteClass() async{
    _setStringList(name: classKey, data: null);
  }

  void setLinkToTimetable(String link) async{
    _setString(name: timetableLinkKey, data: link);
  }

  void setShowMyLessons(bool value){
    _setBool(name: showOnlyMyLessonsKey, data: value);
  }

  void setTheme(String theme) async{
    _setString(name: themeKey, data: theme);
  }

  /*
  GETTERS
   */
  Future<String> _getString({@required name}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = prefs.getString(name);
    return data;
  }

  Future<List<String>> _getStringList({@required name}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> stringList = prefs.getStringList(name);
    return stringList ?? [];
  }

  Future<bool> _getBool({@required String name})async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool value = prefs.getBool(name);
    return value;
  }

  Class _getClassObject({@required List<String> classList}){
    try {
      Class classToReturn = Class.fromList(classList);
      return classToReturn;
    }catch (e){
      return null;
    }
  }

  Future<Class> getClass()async{
    List<String> list = await _getStringList(name: classKey);
    Class _class = _getClassObject(classList: list);
    return _class;
  }

  Future<List> getDownloadedTimetableConf() async{
    List<String> list = await _getStringList(name: downloadedTimetableKey);
    Class _class = _getClassObject(classList: list);
    String timetableUrl = list.isNotEmpty ?  list[2] : null;
    String generateDateText = list.isNotEmpty && list.length >= 4 ?  list[3] : null;
    String beginDateText = list.isNotEmpty && list.length >= 5 ?  list[4] : null;
    return [_class, timetableUrl, generateDateText, beginDateText];
  }

  Future<String> getLinkToTimetable() async{
    return await _getString(name: timetableLinkKey);
  }

  Future<bool> getShowOnlyMyLessons() async{
    return await _getBool(name: showOnlyMyLessonsKey) ?? false;
  }

  Future<String> getTheme() async{
    return await _getString(name: themeKey);
  }
}