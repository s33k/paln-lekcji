import 'dart:convert';
import 'dart:io' as io;

import 'package:html/dom.dart';
import 'package:http/http.dart' as http;
import 'package:timetable/src/code/day.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/helpers/database_helper.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';
import 'package:timetable/src/helpers/file_helper.dart';
import 'dart:async';
import 'package:html/parser.dart';
import 'package:timetable/src/code/class.dart';

class DownloadHelper {
  DBHelper dbHelper = new DBHelper();

  Future<bool> checkSchedule() async {
    Class selectedClass = await SharedPreferencesHelper().getClass();
    String link = await SharedPreferencesHelper().getLinkToTimetable();

    if (link.endsWith("/index.html")) {
      link = link.replaceFirst("/index.html", "");
    }

    final response = await http.get('$link/${selectedClass.path}');
    var valueToReturn;

    if (response.statusCode == 200) {
      FileHelper fileManager = new FileHelper();
      valueToReturn = await fileManager.checkOneness(response.body.toString());
    } else {
      throw io.HttpException;
    }

    return valueToReturn;
  }

  Future downloadSchedule() async {
    DayNames _dayNames = new DayNames();
    Class selectedClass = await SharedPreferencesHelper().getClass();
    String link = await SharedPreferencesHelper().getLinkToTimetable();
    String linkToRequest = link;
    if (linkToRequest.endsWith("/index.html")) {
      linkToRequest = linkToRequest.replaceFirst("/index.html", "/");
    }

    final response = await http.get('$linkToRequest/${selectedClass.path}');

    if (response.statusCode == 200) {
      FileHelper fileManager = new FileHelper();
      fileManager.saveHtml(response.body.toString());
      var body;
      try {
        body = utf8.decode(response.body.codeUnits);
      } catch (error) {
        body = response.body;
      }
      var document = parse(body);
      var table = document.body.getElementsByClassName('tabela').first;

      List rows = table.children.first.children;
      List<Hour> hours = [];
      int hourNumber = 0;
      List<Lesson> lessons = [];

      for (int row = 1; row < rows.length; row++) {
        List rowChildren = rows[row].children;

        for (int column = 0; column < rowChildren.length; column++) {
          String htmlClass = rowChildren[column].classes.first;

          switch (htmlClass) {
            case 'nr':
              hourNumber++;
              break;
            case 'g':
              String hourString = rowChildren[column].innerHtml;
              hourString.replaceAll(' ', '');
              List<String> rawHours = hourString.split('-');
              var startString = rawHours[0].split(':');
              var endString = rawHours[1].split(':');
              hours.add(new Hour(
                  hourNumber,
                  new Duration(
                      hours: int.parse(startString[0]),
                      minutes: int.parse(startString[1])),
                  new Duration(
                      hours: int.parse(endString[0]),
                      minutes: int.parse(endString[1]))));
              break;
            case 'l':
              List brElementsInList = [];
              List<List> lessonsNow = [];

              var columnChildren = rowChildren[column].children.toList();

              columnChildren.forEach((child) {
                if (child.outerHtml == '<br>') {
                  brElementsInList.add(child);
                }
              });

              for (int i = 0; i < brElementsInList.length; i++) {
                if (brElementsInList.length == 0) {
                  break;
                } else {
                  lessonsNow.add(columnChildren.sublist(
                      lessonsNow.isNotEmpty
                          ? columnChildren.indexOf(brElementsInList[i - 1])
                          : 0,
                      columnChildren.indexOf(brElementsInList[i])));
                }
              }

              if (brElementsInList.length > 0) {
                lessonsNow.add(columnChildren.sublist(
                    columnChildren.indexOf(brElementsInList.last) + 1));
              } else {
                lessonsNow.add(columnChildren);
              }

              for (var lesson in lessonsNow) {
                if(column <= 6){
                var hour = hours.last;
                String subject, teacher, room, group;
                String day = _dayNames.english[column - 2];
                RegExp groupRegex = new RegExp('([1-9])(\/)([1-9])');

                setVariables(var variable) {
                  if(variable.classes.isNotEmpty){
                  switch (variable.classes.first) {
                    case 'p':
                      String rawString = variable.innerHtml;
                      List<String> subjectAndGroup = [];

                      if (rawString.indexOf('-') > -1) {
                        subjectAndGroup = rawString.split('-');
                        subject = subjectAndGroup.first;
                        group = subjectAndGroup.last;
                      } else {
                        if (subject == null) {
                          subject = rawString;
                          int groupPointIndex = rowChildren[column]
                                  .innerHtml
                                  .indexOf(
                                      '>-',
                                      rowChildren[column]
                                          .innerHtml
                                          .indexOf(rawString)) +
                              2;
                          int groupPointIndexEnd = rowChildren[column]
                              .innerHtml
                              .indexOf(' ', groupPointIndex);
                          group = rowChildren[column]
                              .innerHtml
                              .substring(groupPointIndex, groupPointIndexEnd);
                          if (!groupRegex.hasMatch(group)) {
                            group = null;
                          }
                        } else {
                          subject = subject + ' ' + rawString;
                        }
                      }
                      break;
                    case 'n':
                      teacher = variable.innerHtml;
                      break;
                    case 's':
                      room = variable.innerHtml;
                      break;
                    default:
                      break;
                  }}
                }

                lesson.forEach((element) {
                  if (element.attributes['style'] != null && element.attributes['style'].contains('font-size')) {
                    var children = element.children;
                    children.forEach(setVariables);
                  } else if (element.outerHtml == '<br>') {
                  } else {
                    setVariables(element);
                  }
                });

                lessons.add(
                    new Lesson(subject, teacher, room, hour, group, day, true));
                }
              }
              break;
          }
        }
      }

      lessons.removeWhere((lesson) => lesson.subject == null);

      var lastTable = document.body.getElementsByTagName('table').last;
      String generateDateText =
          lastTable.children.first.children.first.children.first.innerHtml;
      generateDateText =
          generateDateText.substring(0, generateDateText.indexOf("<br>"));
      generateDateText = generateDateText.replaceAll("\n", "");

      var tds = document.body.getElementsByTagName('td');
      String beginData;
      tds.forEach((td) {
        if (td.attributes.containsKey('align') &&
            td.attributes['align'] == 'left' &&
            td.innerHtml.contains("Obowiązuje")) {
          beginData = td.innerHtml;
          beginData = beginData.replaceAll('\n', "");
        }
      });

      await dbHelper.deleteLessons();
      await dbHelper.deleteHours();
      await dbHelper.insertManyLessons(lessons);
      await dbHelper.insertManyHours(hours);
      await SharedPreferencesHelper().setDownloadedTimetableConf(
          selectedClass, link,
          generateDateText: generateDateText, beginDateText: beginData);
    } else {
      throw (io.HttpException);
    }
  }

  Future<List<Class>> downloadClasses() async {
    String link = await SharedPreferencesHelper().getLinkToTimetable();
    bool isTable = false;
    if (link.endsWith('/')) link.substring(0, link.length - 2);

    if (!link.endsWith('/index.html')) {
      link += '/index.html';
    }
    
    final previewResponse = await http.get(link);
    
    if(previewResponse.statusCode == 200){
      var body = parse(previewResponse.body);
      if(body.getElementsByClassName('menu').isNotEmpty){
        link = link.replaceFirst('/index.html', '/index_o.html');
        isTable = true;
      }else{
        link = link.replaceFirst('/index.html', '/lista.html');
      }
    }else{
      throw "Wyspątpił problem z połączeniem, http status: ${previewResponse.statusCode}";
    }

    final response = await http.get(link);
    List<Class> listOfClasses = [];

    if (response.statusCode == 200) {
      String body;
      try {
        body = utf8.decode(response.body.codeUnits);
      }catch(error){
        body = response.body;
      }
      var document = parse(body);

      if(isTable){
      var table = document.getElementsByClassName('tabela').first;

      table.children.forEach((tr) {
        tr.children.forEach((td) {
          var tag = td.children.forEach((child) {
            listOfClasses.add(new Class(
                number: child.children.first.innerHtml,
                path: child.children.first.attributes['href']));
          });
        });
      });

      }else if (document.getElementsByTagName('select').isNotEmpty) {
        var classesHtml = document.body.getElementsByTagName('select').first;
        classesHtml.children.forEach((element) {
          if (element.attributes.containsKey('value')) {
            String classNumber = element.innerHtml;
            String classPath =
                "plany/o" + element.attributes['value'] + ".html";
            listOfClasses.add(new Class(number: classNumber, path: classPath));
          }
        });
      } else if (document.getElementById('oddzialy') != null) {
        document.getElementById('oddzialy').children.forEach((element) {
          String classPath = element.children.first.attributes['href'];
          String classNumber = element.children.first.innerHtml;
          listOfClasses.add(new Class(number: classNumber, path: classPath));
        });
      } else {
        List<Element> h4s = document.body.getElementsByTagName('h4');
        int headerIndex;

        h4s.forEach((h4){
          if(h4.innerHtml == "Oddziały"){
            headerIndex = document.body.children.indexOf(h4);
          }
        });

        Element classList = document.body.children[headerIndex+1];
        classList
            .children
            .forEach((element) {
          var tag = element.children.first;
          String classPath = tag.attributes['href'];
          String classNumber = tag.innerHtml;

          listOfClasses.add(new Class(number: classNumber, path: classPath));
        });
      }
      return listOfClasses;
    } else {
      throw "Wyspątpił problem z połączeniem, http status: ${response.statusCode}";
    }
  }
}
