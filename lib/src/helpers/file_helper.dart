import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FileHelper{
  bool _oneness;

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;

    return File('$path/LastTimeTable.html');
  }

  Future<bool> checkOneness (var html) async {
    final file = await _localFile;
    String contents;

    try{
      contents = await file.readAsString();
    }catch(e){
      saveHtml(html);
    }

    try {
      contents = await file.readAsString();
      _oneness = contents == html.toString() ? true : false;

      return _oneness;
    } catch (e) {
      return null;
    }
  }

  Future<File> saveHtml(var html) async {
    final file = await _localFile;

    return file.writeAsString(html);
  }
}