import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timetable/src/blocs/schedule/schedule_bloc.dart';
import 'package:timetable/src/blocs/schedule/schedule_events.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/helpers/database_helper.dart';

void main(){
  // run this tests only on device with command "flutter run <PATH>"
  group("schedule bloc tests - ", (){
    ScheduleBloc bloc;

    setUp((){
      SharedPreferences.setMockInitialValues({
        'flutter.Class': ['1i', 'plany/o18.html'],
        'flutter.Timetable link': 'http://plan.zszglogow.pl',
        'flutter.Theme': "AMOLED",
        'flutter.Downloaded timetable conf': ['1i', 'plany/o18.html', 'http://plan.zszglogow.pl'],
        'flutter.Show only my': false,
      });
      DBHelper dbHelper = new DBHelper();
      dbHelper.deleteHours();
      dbHelper.deleteLessons();
      dbHelper.insertManyLessons([
        Lesson('Maths', 'maths teacher', '202',
            Hour(1, Duration(hours: 8), Duration(hours: 8, minutes: 45)), '', 'MONDAY', true),
        Lesson('English', 'english teacher', '305',
            Hour(2, Duration(hours: 8, minutes: 55), Duration(hours: 9, minutes: 40)),'1/2', 'MONDAY', false),
        Lesson('Physic', 'physic teacher', '201',
            Hour(3, Duration(hours: 9, minutes: 50), Duration(hours: 10, minutes: 35)), '', 'MONDAY', true),
      ]);
      dbHelper.insertManyHours([
        Hour(1, Duration(hours: 8), Duration(hours: 8, minutes: 45)),
        Hour(2, Duration(hours: 8, minutes: 55), Duration(hours: 9, minutes: 40)),
        Hour(3, Duration(hours: 9, minutes: 50), Duration(hours: 10, minutes: 35)),
        Hour(4, Duration(hours: 11, minutes: 00), Duration(hours: 11, minutes: 45))
      ]);
      bloc = new ScheduleBloc();
    });

    test("load bloc", ()async{
      bloc.add(LoadSchedule());

      ScheduleModel resultFromBloc = await bloc.state.first;

      expect(resultFromBloc.actuallyUrl, 'http://plan.zszglogow.pl');
      expect(resultFromBloc.actuallyClass.number, '1i');
      expect(resultFromBloc.actuallyClass.path, 'plany/o18.html');
      expect(resultFromBloc.schedule.allHours[0].number, 1);
      expect(resultFromBloc.schedule.allHours[2].end, Duration(hours: 10, minutes: 35));
      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons[0].subject, 'Maths');
    }, skip: true);

    test("show All lessons ", ()async{
      bloc.add(ShowAllLessons());
      bloc.add(LoadSchedule());

      ScheduleModel resultFromBloc = await bloc.state.first;

      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons.length, 3);
    }, skip: true);

    test("show ONLY MY lessons ", ()async{
      bloc.add(ShowOnlyMyLessons());
      bloc.add(LoadSchedule());

      ScheduleModel resultFromBloc = await bloc.state.first;

      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons.length, 2);
    }, skip: true);


    test("insert lesson ", ()async{
      Lesson lessonToInsert = Lesson('eco', 'PK', '303', Hour(4, Duration(), Duration()), '', 'MONDAY', true,);

      bloc.add(ShowAllLessons());
      bloc.add(AddLesson(lessonToInsert));
      bloc.add(LoadSchedule());

      ScheduleModel resultFromBloc = await bloc.state.first;

      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons.length, 4);
      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons[3].subject, 'eco');
      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons[3].teacher, 'PK');
      expect(resultFromBloc.schedule.allDays["MONDAY"].lessons[3].room, '303');
    }, skip: true);

    test("delete lesson", ()async{
      bloc.add(LoadSchedule());
      ScheduleModel resultBloc = await bloc.state.first;
      Lesson lessonToRemove = resultBloc.schedule.allDays["MONDAY"].lessons[2];

      bloc.add(DeleteLesson(lessonToRemove));
      bloc.add(LoadSchedule());
      ScheduleModel finalResultBloc = await bloc.state.first;

      expect(finalResultBloc.schedule.allDays["MONDAY"].lessons.length, 3);
    }, skip: true);

    test("edit lesson", ()async{
      await bloc.add(LoadSchedule());
      ScheduleModel resultBloc = await bloc.state.first;
      Lesson lessonToEdit = resultBloc.schedule.allDays["MONDAY"].lessons[1];

      lessonToEdit = lessonToEdit.copyWith(subject: "Art");
      await bloc.add(EditLesson(lessonToEdit));

      bloc.dispose();
      bloc = ScheduleBloc();
      bloc.add(LoadSchedule());
      ScheduleModel finalResultBloc = await bloc.state.first;

      expect(finalResultBloc.schedule.allDays["MONDAY"].lessons.length, 3);
      expect(finalResultBloc.schedule.allDays["MONDAY"].lessons[1].subject, "Art");
    }, skip: true);

    test("check local save status", ()async{
      bloc.add(CheckScheduleStatus());

      dynamic resultBloc = await bloc.localStatusState.first;

      expect(resultBloc, isNotNull);
      expect(resultBloc, isInstanceOf<bool>());
    }, skip: true);

    test('dispose bloc', (){
      expect(bloc.localStatusState, emitsInOrder([]));
      expect(bloc.state, emitsInOrder([]));

      bloc.dispose();
    }, skip: true);
  });
}