import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timetable/src/blocs/settings/bloc.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';

void main(){
  group('Basics settings bloc tests', (){
    SettingsBloc bloc;

    setUp((){
      SharedPreferences.setMockInitialValues({"flutter.Class": ['1i', 'plany/o18.html'], "flutter.Timetable link": "http://plan.zszglogow.pl"});
      bloc = new SettingsBloc();
    });

    test("Initiazlizing load test", () async{
      dynamic value = await bloc.state.first;

      expect(value.selectedClass.number, "1i");
      expect(value.selectedClass.path, "plany/o18.html");
      expect(value.url, "http://plan.zszglogow.pl");
    });

    test("url validation test", () async{
      expectLater(bloc.urlValidatorState, emitsInOrder(["Nie wprowadzono linku", "Wprowadź poprawy link", true]));

      bloc.addUrlValidatorEvent(CheckUrl(""));
      bloc.addUrlValidatorEvent(CheckUrl("213.234"));
      bloc.addUrlValidatorEvent(CheckUrl("http://plan.zszglogow.pl"));
    });

    test("Change link to plan", () async{
      await bloc.addUrlValidatorEvent(SubmitUrl("http://google.com"));
      dynamic value = await bloc.state.first;

      expect(value.url, "http://google.com");
      expect(value.selectedClass, null);
    });

    test("Get classes list", (){
      expectLater(bloc.classListState, emits(isInstanceOf<List>()));

      bloc.addClassEvent(LoadClassList());
    }, skip: true);

    test("Set class", () async{
      await bloc.addClassEvent(SetClass(Class(number: "1g", path: "plany/o12.html")));
      dynamic value = await bloc.state.first;

      expect(value.selectedClass.number, '1g');
      expect(value.selectedClass.path, 'plany/o12.html');
    });
  });

  group("Settings bloc with sharedpreferences", (){
    SettingsBloc bloc;

    setDefaultPreferences() async{
      SharedPreferences sh = await SharedPreferences.getInstance();
      await sh.setStringList("Class", ['1i', 'plany/o18.html']);
      await sh.setString("Timetable link", "http://plan.zszglogow.pl");
    }

    restartBloc(){
      bloc.dispose();
      bloc = SettingsBloc();
    }

    setUp((){
      SharedPreferences.setMockInitialValues({"flutter.Class": ['1i', 'plany/o18.html'], "flutter.Timetable link": "http://plan.zszglogow.pl"});
      bloc = SettingsBloc();
    });

    test("Bloc return same value as sahredpreferences", () async{
      dynamic value = await bloc.state.first;
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      List<String> _classFromPrefs = _preferences.getStringList("Class");
      String _urlFromPrefs = _preferences.getString("Timetable link");

      expect(value.selectedClass.number, _classFromPrefs[0]);
      expect(value.selectedClass.path, _classFromPrefs[1]);
      expect(value.url, _urlFromPrefs);
    });

    test("Bloc return same value as SharedPreferencesHelper", () async{
      dynamic value = await bloc.state.first;
      SharedPreferencesHelper _preferences = new SharedPreferencesHelper();

      Class _classFromHelper = await _preferences.getClass();
      String _urlFromHelper = await _preferences.getLinkToTimetable();

      expect(value.selectedClass.number, _classFromHelper.number);
      expect(value.selectedClass.path, _classFromHelper.path);
      expect(value.url, _urlFromHelper);
    });

    test("Bloc change class as sharedpreferences", () async{
      await setDefaultPreferences();
      restartBloc();

      dynamic value = await bloc.state.first;
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      List<String> _classFormInitSP = _preferences.getStringList("Class");

      expect(value.selectedClass.number, _classFormInitSP[0]);
      expect(value.selectedClass.path, _classFormInitSP[1]);

      await bloc.addClassEvent(SetClass(Class(number: '2i', path: 'plany/o12.html')));

      dynamic valueAfterChange = await bloc.state.first;
      List<String> classAfterChange = _preferences.getStringList("Class");

      expect(valueAfterChange.selectedClass, isNotNull);
      expect(valueAfterChange.selectedClass.number, classAfterChange[0]);
      expect(valueAfterChange.selectedClass.path, classAfterChange[1]);
    });

    test("Bloc change class as SharedPreferencesHelper", () async{
      await setDefaultPreferences();
      restartBloc();

      dynamic value = await bloc.state.first;
      SharedPreferencesHelper _preferencesHelper = SharedPreferencesHelper();
      Class _classFormInitSP = await _preferencesHelper.getClass();

      expect(value.selectedClass.number, '1i');
      expect(value.selectedClass.number, _classFormInitSP.number);
      expect(value.selectedClass.path, _classFormInitSP.path);

      await bloc.addClassEvent(SetClass(Class(number: '2i', path: 'plany/o12.html')));

      dynamic valueAfterChange = await bloc.state.first;
      Class classAfterChange = await _preferencesHelper.getClass();

      expect(valueAfterChange.selectedClass, isNotNull);
      expect(valueAfterChange.selectedClass.number, '2i');
      expect(valueAfterChange.selectedClass.path, 'plany/o12.html');
      expect(valueAfterChange.selectedClass.number, classAfterChange.number);
      expect(valueAfterChange.selectedClass.path, classAfterChange.path);
    });

    test("Bloc change url as sharedpreferences", ()async{
      await setDefaultPreferences();
      restartBloc();

      dynamic value = await bloc.state.first;
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      String _urlInitSP = _preferences.getString("Timetable link");

      expect(value.url, "http://plan.zszglogow.pl");
      expect(value.url, _urlInitSP);

      await bloc.addUrlValidatorEvent(SubmitUrl("https://google.com"));

      dynamic valueAfterChange = await bloc.state.first;
      String _urlAfterChange = _preferences.getString("Timetable link");
      List<String> _classAfterChange = _preferences.getStringList("Class");

      expect(valueAfterChange.url, "https://google.com");
      expect(valueAfterChange.url, _urlAfterChange);
      expect(valueAfterChange.selectedClass, isNull);
      expect(_classAfterChange, isNull);
    });

    test("Bloc change url as SharedPreferencesHelper", ()async{
      await setDefaultPreferences();
      restartBloc();

      dynamic value = await bloc.state.first;
      SharedPreferencesHelper _preferences = new SharedPreferencesHelper();
      String _urlInitSP = await _preferences.getLinkToTimetable();

      expect(value.url, "http://plan.zszglogow.pl");
      expect(value.url, _urlInitSP);

      await bloc.addUrlValidatorEvent(SubmitUrl("https://google.com"));

      dynamic valueAfterChange = await bloc.state.first;
      String _urlAfterChange = await _preferences.getLinkToTimetable();
      Class _classAfterChange = await _preferences.getClass();

      expect(valueAfterChange.url, "https://google.com");
      expect(valueAfterChange.url, _urlAfterChange);
      expect(valueAfterChange.selectedClass, isNull);
      expect(_classAfterChange, isNull);
    });
  });
}