import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timetable/src/blocs/theme/bloc.dart';

void main(){
  group('Theme bloc', (){
    ThemeBloc bloc;

    _checkType(dynamic value){
      expect(value, isInstanceOf<ThemeModel>());
    }

    setUp((){
      SharedPreferences.setMockInitialValues({'flutter.Theme': 'Default', });
      bloc = new ThemeBloc();
    });

    test("Theme should be loaded", () async{
      dynamic value = await bloc.themeStream.first;

      _checkType(value);
      expect(value.theme, Themes.DEFAULT);
    });

    test("Changing theme", () async{
      await bloc.add(ChangeThemeEvent(Themes.DARK));
      dynamic value = await bloc.themeStream.first;

      _checkType(value);
      expect(value.theme, Themes.DARK);
      expect(value.themeData.brightness, Brightness.dark);
    });
  });
}