import 'package:flutter_test/flutter_test.dart';
import 'package:timetable/src/code/hour.dart';
import 'package:timetable/src/code/lesson.dart';
import 'package:timetable/src/helpers/database_helper.dart';

void main() {
  // run this tests only on device with command "flutter run <PATH>"
  group('database helper tests', () {
    DBHelper dbHelper;

    setUp(() {
      dbHelper = new DBHelper();
      dbHelper.deleteLessons();
      dbHelper.deleteHours();
    });

    test("db should be clear", () async {
      List<dynamic> lessonsList = await dbHelper.getLessons;
      List<dynamic> hoursList = await dbHelper.getHours;

      expect(lessonsList, isEmpty);
      expect(hoursList, isEmpty);
    }, skip: true);

    test("add lesson", () async {
      Lesson lessonToSave = new Lesson('subject', 'teacher', 'room',
          Hour(1, Duration(), Duration()), '', '', true);

      dbHelper.insertLesson(lessonToSave);
      List<dynamic> lessonsList = await dbHelper.getLessons;

      expect(lessonsList, isNotEmpty);
      expect(lessonsList.first.subject, lessonToSave.subject);
      expect(lessonsList.first.teacher, lessonToSave.teacher);
      expect(lessonsList.first.room, lessonToSave.room);
      expect(lessonsList.first.hour.number, lessonToSave.hour.number);
      expect(lessonsList.first.group, lessonToSave.group);
      expect(lessonsList.first.day, lessonToSave.day);
      expect(lessonsList.first.show, lessonToSave.show);
    }, skip: true);

    test("add many lessons", () async {
      List<Lesson> lessonsListToSave = [
        Lesson('Maths', 'maths teacher', '202',
            Hour(1, Duration(hours: 8), Duration(hours: 8, minutes: 45)), '', 'MONDAY', true),
        Lesson('English', 'english teacher', '305',
            Hour(2, Duration(hours: 8, minutes: 55), Duration(hours: 9, minutes: 40)),'1/2', 'MONDAY', false),
        Lesson('Physic', 'physic teacher', '201',
            Hour(3, Duration(hours: 9, minutes: 50), Duration(hours: 10, minutes: 35)), '', 'MONDAY', true),
      ];

      dbHelper.deleteLessons();
      dbHelper.insertManyLessons(lessonsListToSave);
      List<dynamic> resultLessonsList = await dbHelper.getLessons;

      expect(resultLessonsList, isNotEmpty);
      expect(resultLessonsList[0].subject, lessonsListToSave[0].subject);
      expect(resultLessonsList[2].teacher, lessonsListToSave[2].teacher);
      expect(resultLessonsList[2].hour.number, lessonsListToSave[2].hour.number);
      expect(resultLessonsList[1].group, lessonsListToSave[1].group);
    }, skip: true);

    test("add hour", () async {
      Hour hourToSave =
          new Hour(1, Duration(hours: 8), Duration(hours: 8, minutes: 45));

      dbHelper.insertHour(hourToSave);
      List<dynamic> hoursList = await dbHelper.getHours;

      expect(hoursList, isNotEmpty);
      expect(hoursList.first.number, hourToSave.number);
      expect(hoursList.first.start, hourToSave.start);
      expect(hoursList.first.end, hourToSave.end);
    }, skip: true);

    test("add many hours", ()async{
      List<Hour> hoursListToSave = [
        Hour(1, Duration(hours: 8), Duration(hours: 8, minutes: 45)),
        Hour(2, Duration(hours: 8, minutes: 55), Duration(hours: 9, minutes: 40)),
        Hour(3, Duration(hours: 9, minutes: 50), Duration(hours: 10, minutes: 35))
      ];

      dbHelper.deleteHours();
      dbHelper.insertManyHours(hoursListToSave);
      List<dynamic> resultHoursList = await dbHelper.getHours;

      expect(resultHoursList, isNotEmpty);
      expect(resultHoursList[0].start, hoursListToSave[0].start);
      expect(resultHoursList[2].end, hoursListToSave[2].end);
      expect(resultHoursList[1].number, hoursListToSave[1].number);
    }, skip: true);
  });
}
