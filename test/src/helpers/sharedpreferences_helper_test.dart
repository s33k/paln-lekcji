import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timetable/src/code/class.dart';
import 'package:timetable/src/helpers/sharedpreferences_helper.dart';

void main() {
  group("sharedpreferences helper read tests", () {
    setUp(() {
      SharedPreferences.setMockInitialValues({
        'flutter.Class': ['1i', 'plany/o18.html'],
        'flutter.Timetable link': 'http://plan.zszglogow.pl',
        'flutter.Theme': "AMOLED",
        'flutter.Downloaded timetable conf': ['1i', 'plany/o18.html', 'http://plan.zszglogow.pl'],
        'flutter.Show only my': false,
      });
    });

    test("read Class test", () async {
      SharedPreferencesHelper _preferencesHelper =
          new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      Class classFromHelper = await _preferencesHelper.getClass();
      List<String> classFromSH = _preferences.getStringList('Class');

      expect(classFromSH[0], '1i');
      expect(classFromSH[1], 'plany/o18.html');
      expect(classFromHelper.number, classFromSH[0]);
      expect(classFromHelper.path, classFromSH[1]);
    });

    test("read Timetable url test", () async {
      SharedPreferencesHelper _preferencesHelper =
          new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      String urlFromHelper = await _preferencesHelper.getLinkToTimetable();
      String urlFromSH = _preferences.getString('Timetable link');

      expect(urlFromSH, 'http://plan.zszglogow.pl');
      expect(urlFromHelper, urlFromSH);
    });

    test("read Theme test", () async {
      SharedPreferencesHelper _preferencesHelper =
          new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      String themeFromHelper = await _preferencesHelper.getTheme();
      String themeFromSH = _preferences.getString('Theme');

      expect(themeFromSH, 'AMOLED');
      expect(themeFromHelper, themeFromSH);
    });

    test("read Show Only My Lessons test", () async{
      SharedPreferencesHelper _preferencesHelper = new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      bool resultFromHelper = await _preferencesHelper.getShowOnlyMyLessons();
      bool resultFromSH = _preferences.getBool('Show only my');

      expect(resultFromSH, false);
      expect(resultFromSH, resultFromHelper);
    });

    test("read Downloaded Timetable Conf test", () async {
      SharedPreferencesHelper _preferencesHelper =
      new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      List classFromHelper = await _preferencesHelper.getDownloadedTimetableConf();
      List<String> classFromSH = _preferences.getStringList('Downloaded timetable conf');

      expect(classFromSH[0], '1i');
      expect(classFromSH[1], 'plany/o18.html');
      expect(classFromSH[2], 'http://plan.zszglogow.pl');
      expect(classFromHelper[0].number, classFromSH[0]);
      expect(classFromHelper[0].path, classFromSH[1]);
      expect(classFromHelper[1], classFromSH[2]);
    });
  });

  group("sharedpreferences helper write tests", () {
    setDefaults() async {
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      _preferences.setStringList('Class', ['1i', 'plany/o18.html']);
      _preferences.setString('Timetable link', 'http://plan.zszglogow.pl');
      _preferences.setString('Theme', 'AMOLED');
      _preferences.setStringList('Dwonloaded timetable conf', ['1i', 'plany/o18.html', 'http://plan.zszglogow.pl']);
      _preferences.setBool("Show only my", false);
    }

    setUp(() {
      SharedPreferences.setMockInitialValues({
        'flutter.Class': ['1i', 'plany/o18.html'],
        'flutter.Timetable link': 'http://plan.zszglogow.pl',
        'flutter.Theme': "AMOLED",
        'flutter.Downloaded timetable conf': ['1i', 'plany/o18.html', 'http://plan.zszglogow.pl'],
        'flutter.Show only my': false,
      });
    });

    test('set Class test', () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
          new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      Class classToSave = new Class(number: '23g', path: 'plany/o1.html');

      await _preferencesHelper.setClass(classToSave);
      Class classFromHelper = await _preferencesHelper.getClass();
      List<String> classFromSH = _preferences.getStringList("Class");

      expect(classFromHelper.number, classToSave.number);
      expect(classFromHelper.path, classToSave.path);
      expect(classFromHelper.number, classFromSH[0]);
      expect(classFromHelper.path, classFromSH[1]);
    });

    test("clear Class test", () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
          new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();

      await _preferencesHelper.deleteClass();
      Class classFromHelper = await _preferencesHelper.getClass();
      List<String> classFromSH = _preferences.getStringList('Class');

      expect(classFromHelper, isNull);
      expect(classFromSH, isNull);
    });

    test('set Timetable test', () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
      new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      String urlToSave = 'https://google.com';

      await _preferencesHelper.setLinkToTimetable(urlToSave);
      String urlFromHelper = await _preferencesHelper.getLinkToTimetable();
      String urlFromSH = _preferences.getString("Timetable link");

      expect(urlFromHelper, urlToSave);
      expect(urlFromHelper, urlFromSH);
    });

    test('set Theme test', () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
      new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      String themeToSave = 'Dark';

      await _preferencesHelper.setTheme(themeToSave);
      String themeFromHelper = await _preferencesHelper.getTheme();
      String themeFromSH = _preferences.getString("Theme");

      expect(themeFromHelper, themeToSave);
      expect(themeFromHelper, themeFromSH);
    });

    test('set Show Only My test', () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
      new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      bool valueToSave = true;

      await _preferencesHelper.setShowMyLessons(valueToSave);
      bool resultFromHelper = await _preferencesHelper.getShowOnlyMyLessons();
      bool resultFromSH = _preferences.getBool("Show only my");

      expect(resultFromHelper, valueToSave);
      expect(resultFromHelper, resultFromSH);
    });

    test('set Downloaded Timetable Conf test', () async {
      await setDefaults();
      SharedPreferencesHelper _preferencesHelper =
      new SharedPreferencesHelper();
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      Class classToSave = new Class(number: '23g', path: 'plany/o1.html');
      String urlToSave = 'https://google.com';

      await _preferencesHelper.setDownloadedTimetableConf(classToSave, urlToSave);
      List<dynamic> resultFromHelper = await _preferencesHelper.getDownloadedTimetableConf();
      List<String> resultFromSH = _preferences.getStringList("Downloaded timetable conf");

      expect(resultFromHelper[0].number, classToSave.number);
      expect(resultFromHelper[0].path, classToSave.path);
      expect(resultFromHelper[1], urlToSave);
      expect(resultFromHelper[0].number, resultFromSH[0]);
      expect(resultFromHelper[0].path, resultFromSH[1]);
      expect(resultFromHelper[1], resultFromSH[2]);
    });
  });
}
